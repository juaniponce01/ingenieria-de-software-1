!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/6/2023 21:58:41'!
checkTest: aTest customerBookToTest: aCustomerBook numberOfActiveCustomers: activeCustomers numberOfSuspendedCustomers: suspendedCustomers numberOfCustomers: customers condition: aClosure

	aTest assert: activeCustomers equals: aCustomerBook numberOfActiveCustomers.
	aTest assert: suspendedCustomers equals: aCustomerBook numberOfSuspendedCustomers.
	aTest assert: customers equals: aCustomerBook numberOfCustomers.
	aClosure value.! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JP 9/5/2023 17:59:47'!
manageProblem: problem on: errorDescription trying: assertion

	[ problem.
	self fail ]
		on: errorDescription 
		do: assertion! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/7/2023 16:50:26'!
manageProblem: aProblem withError: errorDescription andAssertBoth: conditionToAssert and: anotherConditionToAssert
	
	[aProblem]
		on: errorDescription 
		do: [:anError| self assert: conditionToAssert . self assert: anotherConditionToAssert ]! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JP 9/4/2023 21:58:39'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook |
	
	customerBook := CustomerBook new.
	
	customerBook checkTimeLimit: 50 * millisecond withAction: [self addCustomerNamed: 'John Lennon'].
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JP 9/4/2023 22:00:14'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	  
	customerBook checkTimeLimit: 100 * millisecond withAction: [self removeCustomerNamed: paulMcCartney].
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/7/2023 16:52:25'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	self manageProblem: [customerBook addCustomerNamed: ''. self fail.] 
				withError: Error
				andAssertBoth: [:anError | anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage]
				and: [customerBook isEmpty]
		! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/7/2023 16:54:15'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self manageProblem: [ customerBook removeCustomerNamed: 'Paul McCartney'.
	self fail ]
		withError: NotFound
		andAssertBoth: [customerBook numberOfCustomers = 1]
		and: [(customerBook) includesCustomerNamed: johnLennon]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/6/2023 22:06:42'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self checkTest: self customerBookToTest: customerBook numberOfActiveCustomers:0 numberOfSuspendedCustomers:1 numberOfCustomers:1 
	condition: [customerBook assert: (customerBook includesCustomerNamed: paulMcCartney).].
	
	
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/6/2023 22:00:26'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self checkTest: self customerBookToTest: customerBook numberOfActiveCustomers: 0 numberOfSuspendedCustomers: 0 numberOfCustomers: 0 
	condition: [self deny: (customerBook includesCustomerNamed: paulMcCartney).].


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/7/2023 16:56:38'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	
		
	self manageProblem: [customerBook suspendCustomerNamed: 'George Harrison'.
		self fail] 
		withError: CantSuspend 
		andAssertBoth: [customerBook numberOfCustomers = 1.]
		and: [customerBook includesCustomerNamed: johnLennon]
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MG 9/7/2023 16:57:33'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self manageProblem: [customerBook suspendCustomerNamed: johnLennon.
		self fail]
			withError: CantSuspend 
			andAssertBoth: [customerBook numberOfCustomers = 1]
			and: [customerBook includesCustomerNamed: johnLennon].
	
! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'JP 9/5/2023 17:15:02'!
checkTimeLimit: timeLimit withAction: actionToTime
	| millisecondsBeforeRunning millisecondsAfterRunning |
	
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	actionToTime.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.

	self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < (timeLimit)! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	((active includes: aName) or: [suspended includes: aName]) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
removeCustomerNamed: aName 
 
	1 to: active size do: 
	[ :index |
		aName = (active at: index)
			ifTrue: [
				active removeAt: index.
				^ aName 
			] 
	].

	1 to: suspended size do: 	
	[ :index |
		aName = (suspended at: index)
			ifTrue: [
				suspended removeAt: index.
				^ aName 
			] 
	].
	
	^ NotFound signal.
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/4/2023 17:02:48'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/4/2023 17:02:52'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
