!classDefinition: #MarsRoverTest category: 'MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
locateAMarsRoverAt: aCoordinate pointing: aDirection

	^ MarsRover locatedAt: aCoordinate pointing: aDirection! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test01ShouldBeLocatedAndPointingAtTheSameTime

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: aDirection).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test02ShouldNotBePointingAtAnotherDirection

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  South new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: North new.
	
	self deny: (aMarsRover isLocatedAt: aCoordinate pointingAt: aDirection).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test03ShouldNotBeLocatedAtAnotherCoordinate

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  East new.
	aMarsRover := self locateAMarsRoverAt: 0@0 pointing: aDirection.
	
	self deny: (aMarsRover isLocatedAt: aCoordinate pointingAt: aDirection).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test04ShouldDoNothingWhenThereIsNoCommands

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: ''.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test05MovesRightForwardWhenPointingEast

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  East new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'f'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (1@0) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test06MovesUpForwardWhenPointingNorth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'f'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (0@1) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test07MovesDownForwardWhenPointingSouth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  South new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'f'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (0@-1) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test08MovesLeftForwardWhenPointingWest

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  West new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'f'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (-1@0) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test09MovesRightBackwardsWhenPointingWest

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  West new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'b'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (1@0) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test10MovesLeftBackwardsWhenPointingEast

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  East new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'b'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (-1@0) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test11MovesDownBackwardsWhenPointingNorth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'b'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (0@-1) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test12MovesUpBackwardsWhenPointingSouth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  South new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'b'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate + (0@1) pointingAt: aDirection ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test13LeftRotatesNorthWhenPointingEast

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  East new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'l'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: ( North new) ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test14LeftRotatesWestWhenPointingNorth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'l'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: ( West new) ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test15LeftRotatesSouthWhenPointingWest

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  West new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'l'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: ( South new) ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test16LeftRotatesEastWhenPointingSouth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  South new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'l'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: ( East new) ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test17RightRotatesSouthWhenPointingEast

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  East new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'r'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: ( South new) ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test18RightRotatesEastWhenPointingNorth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'r'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: ( East new) ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:03:24'!
test19RightRotatesWestWhenPointingSouth

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  South new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'r'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: West new ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:03:18'!
test20RightRotatesNorthWhenPointingWest

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 1.
	aDirection :=  West new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'r'.
	
	self assert: (aMarsRover isLocatedAt: aCoordinate pointingAt: North new ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test21ProcessManyValidCommands

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 0.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	aMarsRover processCommands: 'ffblfrffrbbb'.
	
	self assert: (aMarsRover isLocatedAt: (-4@3) pointingAt: East new ).! !

!MarsRoverTest methodsFor: 'test' stamp: 'JP 10/9/2023 10:02:20'!
test22ProcessCommandsUntilInvalidCommand

	| aCoordinate aMarsRover aDirection |
	aCoordinate := 0 @ 0.
	aDirection :=  North new.
	aMarsRover := self locateAMarsRoverAt: aCoordinate pointing: aDirection.
	
	self
		should: [ aMarsRover processCommands: 'ffxblf' ]
		raise: Error 
		withExceptionDo: [ :anError |
			self assert: anError messageText = MarsRover invalidCommandErrorDescription ].
		
	self assert: (aMarsRover isLocatedAt: 0@2 pointingAt: aDirection )! !


!classDefinition: #Direction category: 'MarsRover'!
Object subclass: #Direction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!Direction methodsFor: 'rotation' stamp: 'JP 10/9/2023 10:06:25'!
rotateLeftThisMarsRover: aMarsRover

	self subclassResponsibility .! !

!Direction methodsFor: 'rotation' stamp: 'JP 10/9/2023 10:06:38'!
rotateRightThisMarsRover: aMarsRover

	self subclassResponsibility .! !


!Direction methodsFor: 'movement' stamp: 'JP 10/8/2023 19:56:19'!
moveBackwardsThisMarsRover: aMarsRover
	
	self subclassResponsibility .! !

!Direction methodsFor: 'movement' stamp: 'JP 10/8/2023 19:53:48'!
moveForwardThisMarsRover: aMarsRover
	
	self subclassResponsibility .! !


!classDefinition: #East category: 'MarsRover'!
Direction subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!East methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:00:06'!
rotateLeftThisMarsRover: aMarsRover

	aMarsRover changeDirectionToNorth .! !

!East methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:06:17'!
rotateRightThisMarsRover: aMarsRover

	aMarsRover changeDirectionToSouth .! !


!East methodsFor: 'movement' stamp: 'JP 10/8/2023 19:56:30'!
moveBackwardsThisMarsRover: aMarsRover

	aMarsRover moveACoordinateToTheLeft.! !

!East methodsFor: 'movement' stamp: 'JP 10/8/2023 19:51:46'!
moveForwardThisMarsRover: aMarsRover

	aMarsRover moveACoordinateToTheRight.! !


!classDefinition: #North category: 'MarsRover'!
Direction subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!North methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:01:08'!
rotateLeftThisMarsRover: aMarsRover

	aMarsRover changeDirectionToWest .! !

!North methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:06:28'!
rotateRightThisMarsRover: aMarsRover

	aMarsRover changeDirectionToEast .! !


!North methodsFor: 'movement' stamp: 'JP 10/8/2023 19:56:43'!
moveBackwardsThisMarsRover: aMarsRover

	aMarsRover moveACoordinateDown .! !

!North methodsFor: 'movement' stamp: 'JP 10/8/2023 19:54:20'!
moveForwardThisMarsRover: aMarsRover

	aMarsRover moveACoordinateUp .! !


!classDefinition: #South category: 'MarsRover'!
Direction subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!South methodsFor: 'movement' stamp: 'JP 10/8/2023 19:56:56'!
moveBackwardsThisMarsRover: aMarsRover

	aMarsRover moveACoordinateUp .! !

!South methodsFor: 'movement' stamp: 'JP 10/8/2023 19:54:35'!
moveForwardThisMarsRover: aMarsRover

	aMarsRover moveACoordinateDown .! !


!South methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:01:20'!
rotateLeftThisMarsRover: aMarsRover

	aMarsRover changeDirectionToEast .! !

!South methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:06:39'!
rotateRightThisMarsRover: aMarsRover

	aMarsRover changeDirectionToWest .! !


!classDefinition: #West category: 'MarsRover'!
Direction subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!West methodsFor: 'movement' stamp: 'JP 10/8/2023 19:57:04'!
moveBackwardsThisMarsRover: aMarsRover

	aMarsRover moveACoordinateToTheRight .! !

!West methodsFor: 'movement' stamp: 'JP 10/8/2023 19:54:49'!
moveForwardThisMarsRover: aMarsRover

	aMarsRover moveACoordinateToTheLeft .! !


!West methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:01:32'!
rotateLeftThisMarsRover: aMarsRover

	aMarsRover changeDirectionToSouth .! !

!West methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:06:49'!
rotateRightThisMarsRover: aMarsRover

	aMarsRover changeDirectionToNorth .! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'location direction'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'initialization' stamp: 'JP 10/9/2023 09:55:40'!
initializeLocatedAt: aCoordinate pointing: aDirection 
	
	location := aCoordinate.
	direction := aDirection.! !


!MarsRover methodsFor: 'testing' stamp: 'JP 10/9/2023 09:48:17'!
assertValidCommand: aCommand

	(aCommand = 'f' 
	or: aCommand = 'b' 
	or: aCommand = 'l' 
	or: aCommand = 'r') 
		ifFalse: [self error: self class invalidCommandErrorDescription].

	! !

!MarsRover methodsFor: 'testing' stamp: 'JP 10/8/2023 20:05:09'!
isLocatedAt: aCoordinate pointingAt: aDirection
	
	^ location = aCoordinate and: (direction class = aDirection class) .! !


!MarsRover methodsFor: 'processing' stamp: 'JP 10/9/2023 09:48:30'!
processCommand: aCommand

	aCommand = 'f' ifTrue: [ self moveForward ].
	aCommand = 'b' ifTrue: [ self moveBackwards ].
	aCommand = 'l' ifTrue: [ self rotateLeft ].
	aCommand = 'r' ifTrue: [ self rotateRight ]! !

!MarsRover methodsFor: 'processing' stamp: 'JP 10/9/2023 09:48:44'!
processCommands: aStringOfCommands 

	aStringOfCommands do: [ :command |
		self assertValidCommand: command asString.
		self processCommand: command asString ].! !


!MarsRover methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:00:27'!
changeDirectionToEast

	^ direction := East new.! !

!MarsRover methodsFor: 'rotation' stamp: 'JP 10/8/2023 19:59:03'!
changeDirectionToNorth

	^ direction := North new.! !

!MarsRover methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:00:21'!
changeDirectionToSouth

	^ direction := South new.! !

!MarsRover methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:00:37'!
changeDirectionToWest

	^ direction := West new.! !

!MarsRover methodsFor: 'rotation' stamp: 'JP 10/8/2023 19:59:36'!
rotateLeft

	direction rotateLeftThisMarsRover: self.! !

!MarsRover methodsFor: 'rotation' stamp: 'JP 10/8/2023 20:06:03'!
rotateRight

	^ direction rotateRightThisMarsRover: self.! !


!MarsRover methodsFor: 'movement' stamp: 'JP 10/7/2023 21:46:07'!
moveACoordinateDown

	location := location + (0@-1)! !

!MarsRover methodsFor: 'movement' stamp: 'JP 10/7/2023 21:46:12'!
moveACoordinateToTheLeft

	location := location + (-1@0)! !

!MarsRover methodsFor: 'movement' stamp: 'JP 10/7/2023 21:46:16'!
moveACoordinateToTheRight

	location := location + (1@0)! !

!MarsRover methodsFor: 'movement' stamp: 'JP 10/7/2023 21:46:21'!
moveACoordinateUp

	location := location + (0@1)! !

!MarsRover methodsFor: 'movement' stamp: 'JP 10/8/2023 19:56:07'!
moveBackwards

	direction moveBackwardsThisMarsRover: self.! !

!MarsRover methodsFor: 'movement' stamp: 'JP 10/8/2023 19:55:16'!
moveForward

	direction moveForwardThisMarsRover: self.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'error handling' stamp: 'JP 10/9/2023 09:50:18'!
invalidCommandErrorDescription

	^'There is an invalid command, MarsRover stops processing commands.'! !


!MarsRover class methodsFor: 'instance creation' stamp: 'JP 10/9/2023 09:58:50'!
locatedAt: aCoordinate pointing: aDirection 

	^self new initializeLocatedAt: aCoordinate pointing: aDirection ! !
