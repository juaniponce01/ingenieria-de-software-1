!classDefinition: #PersistentSet category: 'CustomerImporter'!
Set subclass: #PersistentSet
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!PersistentSet methodsFor: 'initialization' stamp: 'HAW 11/14/2023 08:25:31'!
initializeOn: aSession from: aNonPersistentSet

	session := aSession.
	self addAll: aNonPersistentSet ! !


!PersistentSet methodsFor: 'adding' stamp: 'HAW 11/14/2023 08:23:40'!
add: newObject

	super add: newObject.
	session persist: newObject.
	
	^newObject! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PersistentSet class' category: 'CustomerImporter'!
PersistentSet class
	instanceVariableNames: ''!

!PersistentSet class methodsFor: 'instance creation' stamp: 'HAW 11/14/2023 08:24:32'!
on: aSession

	^self on: aSession from: #()! !

!PersistentSet class methodsFor: 'instance creation' stamp: 'HAW 11/14/2023 08:25:00'!
on: aSession from: aNonPersistentSet

	^self new initializeOn: aSession from: aNonPersistentSet
! !


!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'customerSystem'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:22:41'!
test01Import

	CustomerImporter valueFrom: self validImportData into:customerSystem.

	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:19:39'!
test02CustomerRecordCantHaveLessThanFiveFields

	| customerImporter |
	customerImporter := CustomerImporter from: (ReadStream on: 'C,Pepe,Sanchez,D') into:customerSystem.
	
	self 
		should: [customerImporter value]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = customerImporter invalidNumberOfFieldsInCustomerErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 0]! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:19:49'!
test03CustomerRecordCantHaveMoreThanFiveFields

	| customerImporter |
	customerImporter := CustomerImporter from: (ReadStream on: 'C,Pepe,Sanchez,D,1,x') into:customerSystem.
	
	self 
		should: [customerImporter value]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = customerImporter invalidNumberOfFieldsInCustomerErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 0]! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:19:54'!
test04AddressRecordCantHaveMoreThanSixFields

	| customerImporter |
	customerImporter := CustomerImporter from: (ReadStream on: 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs,x') into:customerSystem.
	
	self 
		should: [customerImporter value]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = customerImporter invalidNumberOfFieldsInAddressErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 1.
			self assert: customerSystem numberOfAddresses equals: 0]! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:19:58'!
test05AddressRecordCantHaveLessThanSixFields

	| customerImporter |
	customerImporter := CustomerImporter from: (ReadStream on: 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636') into:customerSystem.
	
	self 
		should: [customerImporter value]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = customerImporter invalidNumberOfFieldsInAddressErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 1.
			self assert: customerSystem numberOfAddresses equals: 0]! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:20:08'!
test06RecordTypeIsCWIthoutMoreCharacters

	| customerImporter |
	customerImporter := CustomerImporter from: (ReadStream on: 'Casa,Pepe,Sanchez,D,22333444') into:customerSystem.
	
	self 
		should: [customerImporter value]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = customerImporter invalidRecordTypeErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 0 ]! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:20:13'!
test07RecordTypeIsAWIthoutMoreCharacters

	| customerImporter |
	customerImporter := CustomerImporter from: (ReadStream on: 'C,Pepe,Sanchez,D,22333444
Address,San Martin,3322,Olivos,1636') into:customerSystem.
	self 
		should: [customerImporter value]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = customerImporter invalidRecordTypeErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 1.
			self assert: customerSystem numberOfAddresses equals: 0.]! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:20:21'!
test08CannotImportAddressWithoutCustomer

	| customerImporter |
	customerImporter := CustomerImporter from: (ReadStream on: 'A,San Martin,3322,Olivos,1636') into:customerSystem.
	
	self 
		should: [customerImporter value]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = customerImporter cannotImportAddressWithoutCustomerErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 0.
			self assert: customerSystem numberOfAddresses equals: 0.]! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/19/2023 16:20:26'!
test09CantImportEmptyLine

	self 
		should: [CustomerImporter from: (ReadStream on: '') into:customerSystem.]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CustomerImporter cannotImportEmptyStreamErrorMessage.
			self assert: customerSystem numberOfCustomers equals: 0 ]! !


!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince

	| importedAddress |

	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:27:57'!
assertCustomerWithIdentificationType: anIdType number: anIdNumber hasFirstName: aFirstName lastName: aLastName

	| importedCustomer |

	importedCustomer := self customerWithIdentificationType: anIdType number: anIdNumber.

	self assert: aFirstName equals: importedCustomer firstName.
	self assert: aLastName equals: importedCustomer lastName.
	self assert: anIdType equals: importedCustomer identificationType.
	self assert: anIdNumber equals: importedCustomer identificationNumber.

	^importedCustomer

	! !

!ImportTest methodsFor: 'assertions' stamp: 'MG 11/19/2023 15:58:50'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: customerSystem numberOfCustomers! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:45'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'C' number: '23-25666777-9' hasFirstName: 'Juan' lastName: 'Perez'.
	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'
	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:05'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'D' number: '22333444' hasFirstName: 'Pepe' lastName: 'Sanchez'.
	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.


	! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'MG 11/22/2023 15:47:22'!
setUp
	
	(Smalltalk at: #Env) = 'Dev' ifTrue: [customerSystem := TransientCustomerSystem new].
	(Smalltalk at:#Env) = 'QA' ifTrue: [customerSystem := PersistentCustomerSystem new]. 
					
! !


!ImportTest methodsFor: 'customer' stamp: 'MG 11/19/2023 16:14:24'!
customerWithIdentificationType: anIdType number: anIdNumber

	^ customerSystem customerWithIdentificationType: anIdType number: anIdNumber.! !


!ImportTest methodsFor: 'test data' stamp: 'HAW 5/22/2022 18:08:08'!
validImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'readStream newCustomer line record newAddress customerSystem'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'MG 11/19/2023 16:18:27'!
initializeFrom: aReadStream into: aCustomerSystem 
	(aReadStream isEmpty) ifTrue: [self error: self class cannotImportEmptyStreamErrorMessage].
	customerSystem := aCustomerSystem.
	readStream := aReadStream.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/16/2023 20:16:56'!
assertIsValidCustomerRecord

	^ (record size ~= 5) ifTrue: [ self error: self invalidNumberOfFieldsInCustomerErrorMessage ]! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/16/2023 19:41:27'!
createRecord

	^ record := line findTokens: $,! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/16/2023 19:29:57'!
hasLineToImport

	line := readStream nextLine. 
	^line notNil! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/16/2023 20:20:26'!
importAddressData

	(record size ~= 6) ifTrue: [ self error: self invalidNumberOfFieldsInAddressErrorMessage ].
	newAddress := Address new.
	newCustomer addAddress: newAddress.
	newAddress streetName: record second.
	newAddress streetNumber: record third asNumber .
	newAddress town: record fourth.
	newAddress zipCode: record fifth asNumber .
	newAddress province: record sixth! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/19/2023 15:44:27'!
importCustomerData

	self assertIsValidCustomerRecord.
	newCustomer := Customer new.
	newCustomer firstName: record second.
	newCustomer lastName: record third.
	newCustomer identificationType: record fourth.
	newCustomer identificationNumber: record fifth.
	customerSystem addCustomer: newCustomer! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/19/2023 13:22:48'!
importRecord
	
	(self isCustomerRecord) ifTrue: [ ^self importCustomerData ].
	(self isAddressRecord) ifTrue: [ ^ self importAddressData ].
	self error: self invalidRecordTypeErrorMessage.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/19/2023 13:24:49'!
isAddressRecord

	^ line beginsWith: 'A,'! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/19/2023 13:22:06'!
isCustomerRecord

	^ line beginsWith: 'C,'.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/19/2023 16:23:13'!
timportCustomerData

	self assertIsValidCustomerRecord.
	newCustomer := Customer new.
	newCustomer firstName: record second.
	newCustomer lastName: record third.
	newCustomer identificationType: record fourth.
	newCustomer identificationNumber: record fifth.
	customerSystem addCustomer: newCustomer.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'MG 11/19/2023 15:00:50'!
value
	self hasLineToImport ifTrue: [self createRecord.
		self isAddressRecord ifTrue:
					[self error: self cannotImportAddressWithoutCustomerErrorMessage ].
		self importRecord.].
	
	[ self hasLineToImport ] whileTrue: [
		self createRecord.
		self importRecord.
	].

	! !


!CustomerImporter methodsFor: 'error handling' stamp: 'MG 11/19/2023 13:37:48'!
cannotImportAddressWithoutCustomerErrorMessage
	^'Cannot Import an Address without a customer'! !

!CustomerImporter methodsFor: 'error handling' stamp: 'MG 11/16/2023 20:16:40'!
invalidNumberOfFieldsInAddressErrorMessage
	
	^'Number of fields in address must be six'! !

!CustomerImporter methodsFor: 'error handling' stamp: 'MG 11/16/2023 20:16:56'!
invalidNumberOfFieldsInCustomerErrorMessage
	
	^'Number of fields must be five'! !

!CustomerImporter methodsFor: 'error handling' stamp: 'MG 11/19/2023 13:22:48'!
invalidRecordTypeErrorMessage
	^'Identification type should be C or A'! !

!CustomerImporter methodsFor: 'error handling' stamp: 'MG 11/19/2023 13:46:43'!
isAddressWithoutCustomer: aReadStream 
	^ self isAddressRecord ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'MG 11/19/2023 16:19:19'!
from: aReadStream into: aCustomerSystem 
	^self new initializeFrom: aReadStream into: aCustomerSystem! !


!CustomerImporter class methodsFor: 'importing' stamp: 'MG 11/19/2023 16:22:12'!
valueFrom: aReadStream into: aCustomerSystem 

	^(self from: aReadStream into: aCustomerSystem) value! !


!CustomerImporter class methodsFor: 'error handling' stamp: 'MG 11/19/2023 13:43:27'!
cannotImportAddressWithoutCustomerErrorMessage
	^'Cannot import an address without a customer.'! !

!CustomerImporter class methodsFor: 'error handling' stamp: 'MG 11/19/2023 13:16:08'!
cannotImportEmptyStreamErrorMessage
	^'Cannot import an empty Stream'! !


!classDefinition: #CustomerSystem category: 'CustomerImporter'!
Object subclass: #CustomerSystem
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:07:08'!
addCustomer: aNewCustomer

	self subclassResponsibility! !

!CustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:07:18'!
customerWithIdentificationType: anIdType number: anIdNumber

	self subclassResponsibility! !

!CustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:20:42'!
initialize

	self subclassResponsibility! !

!CustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:11:22'!
numberOfAddresses

	self subclassResponsibility! !

!CustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:11:29'!
numberOfCustomers

	self subclassResponsibility! !


!classDefinition: #PersistentCustomerSystem category: 'CustomerImporter'!
CustomerSystem subclass: #PersistentCustomerSystem
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 15:49:42'!
addCustomer: aNewCustomer 
	^session persist: aNewCustomer .! !

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 16:05:59'!
beginTransaction

	^session beginTransaction.! !

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 16:06:19'!
close
	^session close.! !

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 16:06:10'!
commit
	^session commit .! !

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 16:14:37'!
customerWithIdentificationType: anIdType number: anIdNumber 
	^(session
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 16:15:34'!
initialize

	session:= DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.! !

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 16:01:10'!
numberOfAddresses
	^(session selectAllOfType: Address) size.! !

!PersistentCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/19/2023 15:59:32'!
numberOfCustomers
	^(session selectAllOfType: Customer) size! !


!classDefinition: #TransientCustomerSystem category: 'CustomerImporter'!
CustomerSystem subclass: #TransientCustomerSystem
	instanceVariableNames: 'customers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!TransientCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:21:46'!
addCustomer: aNewCustomer

	^customers add: aNewCustomer .! !

!TransientCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:26:57'!
customerWithIdentificationType: anIdType number: anIdNumber

	^(customers select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]) anyOne.! !

!TransientCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 20:22:02'!
initialize

	customers := OrderedCollection new.! !

!TransientCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 21:59:00'!
numberOfAddresses

	^ customers sum: [:aCustomer| aCustomer addresses size] ifEmpty: 0.! !

!TransientCustomerSystem methodsFor: 'as yet unclassified' stamp: 'MG 11/20/2023 21:49:55'!
numberOfCustomers

	^customers size.! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id inTransaction closed'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 11/14/2023 08:52:25'!
beginTransaction

	inTransaction := true.! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 11/14/2023 08:52:18'!
commit

	inTransaction := false.! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 11/14/2023 08:52:30'!
close

	closed := true.! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:49:30'!
assertCanUseDatabase

	self assertIsOpen.
	self assertInTransaction ! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:48:43'!
assertInTransaction

	inTransaction ifFalse: [ self error: 'Not in transaction' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:48:16'!
assertIsOpen

	closed ifTrue: [ self error: 'Connection with database closed' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 09:09:07'!
assertTypeIsPersisted: aType

	(configuration includes: aType) ifFalse: [ self error: 'Object of type ', aType name, ' are not configured to be persisted' ].! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:59'!
isRelationToPersist: possibleRelation

	^ possibleRelation class = Set! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:59'!
persistPossibleRelationOf: anObject at: anInstVarOffset

	| possibleRelation |
		
	possibleRelation := anObject instVarAt: anInstVarOffset.
	(self isRelationToPersist: possibleRelation) ifTrue: [ self persistRelationOf: anObject at: anInstVarOffset with: possibleRelation ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:03'!
persistRelationOf: anObject at: anInstVarOffset with: aRelation

	| persistentRelation |
	
	persistentRelation := PersistentSet on: self from: aRelation.
	anObject instVarAt: anInstVarOffset put: persistentRelation! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 08:58:34'!
persistRelationsOf: anObject

	anObject class instVarNamesAndOffsetsDo: [ :anInstVarName :anInstVarOffset | self persistPossibleRelationOf: anObject at: anInstVarOffset]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 11/14/2023 09:06:24'!
tableOfType: aType

	^ tables at: aType ifAbsentPut: [ Set new ]! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 11/14/2023 08:44:19'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.
	inTransaction := false.
	closed := false.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 11/14/2023 09:06:24'!
persist: anObject

	| table |

	self assertCanUseDatabase.
	self assertTypeIsPersisted: anObject class.
	self delay.
	
	table := self tableOfType: anObject class.
	self defineIdOf: anObject.
	table add: anObject.
	self persistRelationsOf: anObject.
! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 11/14/2023 09:06:56'!
select: aCondition ofType: aType

	self assertCanUseDatabase.
	self assertTypeIsPersisted: aType.
	self delay.
	
	^(self tableOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 11/14/2023 09:07:12'!
selectAllOfType: aType

	self assertCanUseDatabase.
	self assertTypeIsPersisted: aType.
	self delay.
	
	^(self tableOfType: aType) copy ! !


!DataBaseSession methodsFor: 'as yet unclassified' stamp: 'MG 11/16/2023 20:19:52'!
numberOfAddresses
	
	^(self selectAllOfType: Address) size! !

!DataBaseSession methodsFor: 'as yet unclassified' stamp: 'MG 11/16/2023 19:57:58'!
numberOfCustomers
	
	^(self selectAllOfType: Customer) size.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
