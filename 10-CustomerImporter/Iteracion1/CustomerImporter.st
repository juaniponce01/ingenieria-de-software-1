!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session inputStream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'support' stamp: 'MG 11/15/2023 22:21:24'!
importCustomers

	"
	self importCustomers
	"
	|   newCustomer line |

	line := inputStream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fifth.
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData sixth ].

		line := inputStream nextLine. ].


	inputStream close.! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'MG 11/15/2023 23:18:53'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
	
	! !

!ImportTest methodsFor: 'setUp/tearDown' stamp: 'MG 11/13/2023 20:33:19'!
tearDown

	session commit.
	session close! !


!ImportTest methodsFor: 'tests' stamp: 'MG 11/15/2023 23:18:58'!
test01CustomersAndAddressesFromCustomersWereImportedCorrectly
	
	| firstCustomer secondCustomer numberOfCustomers|	
	
	inputStream :=(InputStreamImporter with: 'input.txt') import.
	self importCustomers.
	numberOfCustomers:= (session selectAllOfType: Customer) size.
	
	self assert: numberOfCustomers  equals: 2.
	
	firstCustomer := self selectCustomerWithIdentificationType: 'D' andIdentificationNumber: '22333444'.
	self assertDataFor: firstCustomer onFirstName: 'Pepe' lastName: 'Sanchez' identificationNumber: '22333444' identificationType: 'D'.
	
	self assert: firstCustomer addresses size equals: 2. 
	self assertAddressDataFor: firstCustomer onStreetName: 'Maipu' streetNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.	
	self assertAddressDataFor: firstCustomer onStreetName: 'San Martin' streetNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.	
				
	secondCustomer := self selectCustomerWithIdentificationType: 'C' andIdentificationNumber: '23-25666777-9'.
	self assertDataFor: secondCustomer onFirstName: 'Juan' lastName: 'Perez' identificationNumber: '23-25666777-9' identificationType: 'C'.
	self assert: secondCustomer addresses size equals: 1. 
	self assertAddressDataFor: secondCustomer onStreetName: 'Alem' streetNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'.	! !

!ImportTest methodsFor: 'tests' stamp: 'MG 11/15/2023 23:19:37'!
test02AllCustomersWereSavedInDataBaseSessionWithAStreamOfString
	
	| numberOfCustomers firstCustomer secondCustomer |
	
	inputStream :=(InputStreamImporter with: 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA') import.
	self importCustomers.
	
	numberOfCustomers:= (session selectAllOfType: Customer) size.
	
	self assert: numberOfCustomers  equals: 2.
	
	firstCustomer := self selectCustomerWithIdentificationType: 'D' andIdentificationNumber: '22333444'.
	self assertDataFor: firstCustomer onFirstName: 'Pepe' lastName: 'Sanchez' identificationNumber: '22333444' identificationType: 'D'.
	
	self assert: firstCustomer addresses size equals: 2. 
	self assertAddressDataFor: firstCustomer onStreetName: 'Maipu' streetNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.	
	self assertAddressDataFor: firstCustomer onStreetName: 'San Martin' streetNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.	
				
	secondCustomer := self selectCustomerWithIdentificationType: 'C' andIdentificationNumber: '23-25666777-9'.
	self assertDataFor: secondCustomer onFirstName: 'Juan' lastName: 'Perez' identificationNumber: '23-25666777-9' identificationType: 'C'.
	self assert: secondCustomer addresses size equals: 1. 
	self assertAddressDataFor: secondCustomer onStreetName: 'Alem' streetNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'.	! !


!ImportTest methodsFor: 'testing' stamp: 'MG 11/15/2023 23:06:30'!
assertAddressDataFor: aCustomer onStreetName: aStreetName streetNumber: aStreetNumber town: aTownName zipCode: aZipCodeNumber province: aProvinceName    

	^ self assert: (aCustomer addresses 
				anySatisfy: [ :address | 
					address streetName = aStreetName
					and: [address streetNumber = aStreetNumber ] 
					and: [address town = aTownName ] 
					and: [address zipCode = aZipCodeNumber and: [address province = aProvinceName ]]])! !

!ImportTest methodsFor: 'testing' stamp: 'MG 11/15/2023 23:14:23'!
assertDataFor: aCustomer onFirstName: aFirstName lastName: aLastName identificationNumber: anIdentificationNumber identificationType: anIdentificationType

	self assert: aCustomer firstName equals: aFirstName .
	self assert: aCustomer lastName equals: aLastName .
	self assert: aCustomer identificationNumber equals: anIdentificationNumber .
	self assert: aCustomer identificationType equals: anIdentificationType .! !

!ImportTest methodsFor: 'testing' stamp: 'MG 11/15/2023 22:53:28'!
selectCustomerWithIdentificationType: anIdentificationType andIdentificationNumber: anIdentificationNumber 

	^ (session select: [:aCustomer | aCustomer identificationType = anIdentificationType and: [aCustomer identificationNumber = anIdentificationNumber ]]
	ofType: Customer) anyOne.! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !


!classDefinition: #InputStreamImporter category: 'CustomerImporter'!
Object subclass: #InputStreamImporter
	instanceVariableNames: 'inputStream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!InputStreamImporter methodsFor: 'initialization' stamp: 'MG 11/15/2023 22:29:43'!
initializeWith: anInputStream
	inputStream := anInputStream.! !


!InputStreamImporter methodsFor: 'evaluating' stamp: 'MG 11/15/2023 22:29:43'!
import
	
	
	(inputStream includes: $.) 
		ifTrue: [^UniFileStream new open: inputStream forWrite: false]
		ifFalse: [	^ReadStream on: inputStream from: 0 to: inputStream size].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'InputStreamImporter class' category: 'CustomerImporter'!
InputStreamImporter class
	instanceVariableNames: ''!

!InputStreamImporter class methodsFor: 'instance creation' stamp: 'MG 11/15/2023 22:29:43'!
with: anInputStream
	^self new initializeWith: anInputStream! !
