!classDefinition: #PortfolioTests category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTests
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:17:27'!
portfolioWithAnAccountWithDeposit: aDeposit

	| anAccount |
	anAccount := ReceptiveAccount  new.
	Deposit register: aDeposit on: anAccount.
	
	^ Portfolio with: anAccount.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:17:32'!
portfolioWithAnEmptyAccount
	
	^ Portfolio with: ReceptiveAccount  new .! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:17:35'!
portfolioWithPortfolioWithAnEmptyAccount

	^ Portfolio with: self portfolioWithAnEmptyAccount.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 13:28:06'!
test01PortfolioWithAnEmptyAccount

	|aPortfolio |
	
	aPortfolio := self portfolioWithAnEmptyAccount .
	
	self assert: aPortfolio balance equals: 0.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 13:30:40'!
test02PortfolioWithAnAccountWithADeposit

	|aPortfolio |
	
	aPortfolio := self portfolioWithAnAccountWithDeposit: 100.
	
	self assert: aPortfolio balance equals: 100.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:15:31'!
test03PortfolioWithTwoAccountsWithDeposit

	|aPortfolio anotherAccount |
	
	aPortfolio := self portfolioWithAnAccountWithDeposit: 100.
	anotherAccount :=ReceptiveAccount new.
	Deposit register: 10 on: anotherAccount .
	aPortfolio add: anotherAccount.
	
	self assert: aPortfolio balance equals: 110.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 13:35:52'!
test04PortfolioWithAPortfolioWithAnEmptyAccountHas0Balance

	|aPortfolioWithAPortfolio |
	
	aPortfolioWithAPortfolio := self portfolioWithPortfolioWithAnEmptyAccount.
	
	self assert: aPortfolioWithAPortfolio balance equals: 0.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:17:44'!
test05PortfolioWithTwoPortfoliosWithDeposits

	|aPortfolio aPortfolioWithPortfolios anotherPortfolio |
	
	aPortfolio := self portfolioWithAnAccountWithDeposit: 100.
	anotherPortfolio := self portfolioWithAnAccountWithDeposit: 200.
	
	aPortfolioWithPortfolios :=Portfolio with: aPortfolio .
	aPortfolioWithPortfolios add: anotherPortfolio .
	
	self assert: aPortfolioWithPortfolios balance equals: 300.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:17:48'!
test07PortfolioWithAccountKnowsRegisteredTransactions 

	|aPortfolio anAccount deposit withdraw|
	
	anAccount := ReceptiveAccount  new.
	deposit := Deposit register: 100 on: anAccount.
	withdraw := Withdraw register: 50 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	
	self assert: (aPortfolio hasRegistered: deposit).
	self assert: (aPortfolio hasRegistered: withdraw).! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:17:54'!
test08PortfolioWithPortfolioKnowsRegisteredTransactions 

	|aPortfolio anAccount deposit withdraw portfolioWithPortfolio |
	
	anAccount := ReceptiveAccount  new.
	deposit := Deposit register: 100 on: anAccount.
	withdraw := Withdraw register: 50 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	
	portfolioWithPortfolio := Portfolio with: aPortfolio.
	
	self assert: (aPortfolio hasRegistered: deposit).
	self assert: (aPortfolio hasRegistered: withdraw).! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:18:02'!
test09PortfolioWithAccountAndPortfolioKnowsRegisteredTransactions 

	|aPortfolio anAccount deposit withdraw anotherAccount anotherDeposit anotherPortfolio |
	
	anAccount := ReceptiveAccount  new.
	anotherAccount := ReceptiveAccount new.
	deposit := Deposit register: 100 on: anAccount.
	withdraw := Withdraw register: 50 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	anotherDeposit := Deposit register: 150 on: anotherAccount.
	
	anotherPortfolio := Portfolio with: anotherAccount.
	aPortfolio add: anotherPortfolio.
	
	self assert: (aPortfolio hasRegistered: deposit).
	self assert: (aPortfolio hasRegistered: withdraw).
	self assert: (aPortfolio hasRegistered: anotherDeposit).! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:18:07'!
test10PortfolioDoNotKnowNotRegisteredTransactions

	| aPortfolio anAccount deposit withdraw |
	
	anAccount := ReceptiveAccount  new.
	deposit := Deposit for: 100.
	withdraw := Withdraw for: 50.
	aPortfolio := Portfolio with: anAccount.
			
	self deny: (aPortfolio hasRegistered: deposit).
	self deny: (aPortfolio hasRegistered: withdraw).! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:18:18'!
test10PortfolioWithEmptyAccountKnowsItsTransactions 

	| aPortfolio anAccount |
	
	anAccount := ReceptiveAccount  new.
	aPortfolio := Portfolio with: anAccount.
	
	self assert: 0 equals: aPortfolio transactions size.! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:18:18'!
test11PortfolioWithAccountWithDepositKnowsItsTransactions 

	| aPortfolio anAccount deposit |
	
	anAccount := ReceptiveAccount  new.
	deposit := Deposit register: 100 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	
	self assert: 1 equals: aPortfolio transactions size.
	self assert: (aPortfolio hasRegistered: deposit).! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:18:18'!
test12PortfolioWithPortfolioAndAccountWithDepositsKnowsItsTransactions 

	| aPortfolio anAccount deposit anotherAccount anotherPortfolio anotherDeposit |
	
	anAccount := ReceptiveAccount  new.
	deposit := Deposit register: 100 on: anAccount.
	anotherAccount := ReceptiveAccount new.
	anotherDeposit := Deposit register: 200 on: anotherAccount.
	aPortfolio := Portfolio with: anAccount.
	anotherPortfolio := Portfolio with: anotherAccount.
	
	aPortfolio add: anotherPortfolio.
	
	self assert: 2 equals: aPortfolio transactions size.
	self assert: (aPortfolio hasRegistered: deposit).
	self assert: (aPortfolio hasRegistered: anotherDeposit).! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:18:18'!
test13PortfolioWithManyAccountsAndPortfoliosWithDepositsKnowsItsTransactions 

	| aPortfolio account1 deposit1 account2 anotherPortfolio deposit2 account3 account4 deposit3 deposit4 |
	
	account1 := ReceptiveAccount  new.
	deposit1 := Deposit register: 100 on: account1.
	account2 := ReceptiveAccount new.
	deposit2 := Deposit register: 200 on: account2.
	account3 := ReceptiveAccount new.
	deposit3 := Deposit register: 50 on: account3.
	account4 := ReceptiveAccount new.
	deposit4 := Deposit register: 250 on: account4.
	aPortfolio := Portfolio with: account1.
	anotherPortfolio := Portfolio with: account2.
	
	anotherPortfolio add: account4.
	aPortfolio add: anotherPortfolio.
	aPortfolio add: account3.
	
	self assert: 4 equals: aPortfolio transactions size.
	self assert: (aPortfolio hasRegistered: deposit1).
	self assert: (aPortfolio hasRegistered: deposit2).
	self assert: (aPortfolio hasRegistered: deposit3).
	self assert: (aPortfolio hasRegistered: deposit4).! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:19:52'!
test14PortfolioCantAddItself 

	| aPortfolio anAccount anotherPortfolio |
	
	anAccount := ReceptiveAccount  new.
	Deposit register: 100 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	anotherPortfolio := Portfolio with: anAccount.
	
	self 
		should: [aPortfolio add: aPortfolio ] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Portfolio duplicatedEntityErrorDescription].! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:19:52'!
test15PortfolioCantAddTheSameAccountTwice

	| aPortfolio anAccount deposit |
	
	anAccount := ReceptiveAccount  new.
	deposit := Deposit register: 100 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	
	self 
		should: [aPortfolio add: anAccount ] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Portfolio duplicatedEntityErrorDescription].! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:19:52'!
test16PortfolioCantAddTheSamePortfolioTwice 

	| aPortfolio anAccount anotherPortfolio |
	
	anAccount := ReceptiveAccount  new.
	Deposit register: 100 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	anotherPortfolio := Portfolio with: anAccount.
	
	aPortfolio add: anotherPortfolio.
	
	self 
		should: [aPortfolio add: anotherPortfolio ] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Portfolio duplicatedEntityErrorDescription].! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:19:52'!
test17PortfolioCantAddADuplicatedAccountFromAnotherPortfolio

	| aPortfolio anAccount anotherPortfolio |
	
	anAccount := ReceptiveAccount  new.
	Deposit register: 100 on: anAccount.
	aPortfolio := Portfolio with: anAccount.
	anotherPortfolio := Portfolio with: aPortfolio.
	
	self 
		should: [anotherPortfolio add: anAccount ] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Portfolio duplicatedEntityErrorDescription].! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:19:52'!
test18PortfolioCantAddADuplicatedPortfolioFromAnotherPortfolio

	| anAccount portfolio1 portfolio2 portfolio3 |
	
	anAccount := ReceptiveAccount  new.
	Deposit register: 100 on: anAccount.
	portfolio1 := Portfolio with: anAccount.
	portfolio2 := Portfolio with: portfolio1.
	portfolio3 := Portfolio with: portfolio2.
	
	self 
		should: [portfolio3 add: portfolio1 ] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Portfolio duplicatedEntityErrorDescription].! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 14:26:24'!
test19PortfolioCantAddAPortfolioThatHasItself

	| portfolio1 portfolio2 |
	
	portfolio1 := self portfolioWithAnAccountWithDeposit: 100.
	portfolio2 := self portfolioWithAnAccountWithDeposit: 50.
	
	portfolio1 add: portfolio2.
	
	self 
		should: [portfolio2 add: portfolio1] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Portfolio duplicatedEntityErrorDescription].! !

!PortfolioTests methodsFor: 'tests' stamp: 'JP 10/19/2023 16:27:12'!
test20PortfolioCantAddAnAccountThatIsInPortfolioAbove

	| portfolio1 portfolio2 anAccount |
	
	anAccount := ReceptiveAccount new.
	Deposit register: 100 on: anAccount.
	portfolio1 := Portfolio with: anAccount.
	portfolio2 := self portfolioWithAnAccountWithDeposit: 50.
	
	portfolio1 add: portfolio2.
	
	self 
		should: [portfolio2 add: anAccount] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Portfolio duplicatedEntityErrorDescription].! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:52'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'MG 10/9/2023 21:54:35'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/17/2021 17:29:53'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:14:01'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'MG 10/9/2023 21:12:44'!
test08WithdrawValueShouldNotBeNegative

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := -50.
	
	self 
		should: [Withdraw register: withdrawValue on: account] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Withdraw negativeValueErrorDescription]
	
	
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'MG 10/10/2023 16:58:13'!
test09DepositValueShouldNotBeNegative

	| account depositValue |
	
	account := ReceptiveAccount new.
	depositValue := -50.
	
	self 
		should: [Deposit register: depositValue on: account] 
		raise: Error 
		withExceptionDo: [:anError | 
			self assert: anError messageText = Deposit negativeValueErrorDescription]
	
	
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'initialization' stamp: 'MG 10/10/2023 17:03:03'!
checkValueIsPositive: aValue

	^ aValue < 0 ifTrue: [self error: super class negativeValueErrorDescription]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!


!AccountTransaction class methodsFor: 'instance creation' stamp: 'MG 10/9/2023 21:13:45'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'JP 10/18/2023 14:25:48'!
addToBalance: sum 
	^sum + value.! !

!Deposit methodsFor: 'initialization' stamp: 'MG 10/10/2023 17:01:54'!
initializeFor: aValue
	self checkValueIsPositive: aValue.
	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'MG 10/9/2023 21:13:59'!
for: aValue

	^ self new initializeFor: aValue ! !

!Deposit class methodsFor: 'instance creation' stamp: 'MG 10/10/2023 16:58:50'!
negativeValueErrorDescription

	^'The value must be positive'.! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'MG 10/10/2023 17:02:39'!
initializeFor: aValue

	self checkValueIsPositive: aValue .

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !


!Withdraw methodsFor: 'as yet unclassified' stamp: 'MG 10/10/2023 16:12:16'!
addToBalance: sum 
	^sum - value.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'MG 10/9/2023 21:12:08'!
for: aValue

	^ self new initializeFor: aValue ! !

!Withdraw class methodsFor: 'instance creation' stamp: 'MG 10/9/2023 21:14:43'!
negativeValueErrorDescription

	^'The value must be positive'.! !


!classDefinition: #FinanceService category: 'Portfolio-Ejercicio'!
Object subclass: #FinanceService
	instanceVariableNames: 'entities'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!FinanceService methodsFor: 'as yet unclassified' stamp: 'JP 10/19/2023 00:19:23'!
balance 

	self subclassResponsibility .! !

!FinanceService methodsFor: 'as yet unclassified' stamp: 'JP 10/19/2023 00:19:58'!
hasRegistered: aTransaction

	self subclassResponsibility .! !

!FinanceService methodsFor: 'as yet unclassified' stamp: 'JP 10/19/2023 00:20:23'!
transactions

	self subclassResponsibility .! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
FinanceService subclass: #Portfolio
	instanceVariableNames: 'entityParent'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'balance' stamp: 'JP 10/19/2023 00:42:37'!
balance

	^ entities sum: [ :entity | entity balance ] ifEmpty: 0.! !


!Portfolio methodsFor: 'testing' stamp: 'JP 10/19/2023 00:42:16'!
hasRegistered: aTransaction 

	^entities anySatisfy: [ :entity | entity hasRegistered: aTransaction ].! !

!Portfolio methodsFor: 'testing' stamp: 'JP 10/19/2023 16:03:06'!
includes: anEntity
	
	^ (self = anEntity) or: (entities anySatisfy: [ :entity | entity includes: anEntity ]).! !


!Portfolio methodsFor: 'associating' stamp: 'JP 10/19/2023 16:23:56'!
add: aPotentialNewEntity

	self checkIsNewEntity: aPotentialNewEntity.
	entities add: aPotentialNewEntity.
	aPotentialNewEntity addParent: self.! !

!Portfolio methodsFor: 'associating' stamp: 'JP 10/19/2023 16:04:11'!
checkIsNewEntity: aPotentialNewEntity

	^ ( entityParent checkIsNewEntity: aPotentialNewEntity ) 
		ifTrue: [self error: self class duplicatedEntityErrorDescription].! !


!Portfolio methodsFor: 'transactions' stamp: 'JP 10/19/2023 00:42:26'!
transactions

	^entities 
		inject: OrderedCollection new 
		into: [ :totalEntityTransactions :entity | totalEntityTransactions addAll: entity transactions; yourself ].! !


!Portfolio methodsFor: 'initialization' stamp: 'JP 10/19/2023 15:52:27'!
addParent: anEntityParent

	entityParent := anEntityParent.! !

!Portfolio methodsFor: 'initialization' stamp: 'JP 10/19/2023 16:14:06'!
initializeWith: anEntity

	anEntity addParent: self.	
	entities := OrderedCollection with: anEntity.
	entityParent := PortfolioRoot with: self.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'instance creation' stamp: 'JP 10/19/2023 14:19:39'!
duplicatedEntityErrorDescription

	^'Entity already in Portfolio'.! !

!Portfolio class methodsFor: 'instance creation' stamp: 'JP 10/19/2023 14:09:08'!
with: anEntity
	
	^self new initializeWith: anEntity! !


!classDefinition: #PortfolioRoot category: 'Portfolio-Ejercicio'!
FinanceService subclass: #PortfolioRoot
	instanceVariableNames: 'root'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioRoot methodsFor: 'testing' stamp: 'JP 10/19/2023 16:15:25'!
checkIsNewEntity: anEntity
	
	^root includes: anEntity.! !

!PortfolioRoot methodsFor: 'testing' stamp: 'JP 10/19/2023 16:29:18'!
hasRegistered: aTransaction
	
	^root hasRegistered: aTransaction ! !


!PortfolioRoot methodsFor: 'balance' stamp: 'JP 10/19/2023 16:28:50'!
balance
	
	^root balance! !


!PortfolioRoot methodsFor: 'transactions' stamp: 'JP 10/19/2023 16:29:39'!
transactions
	
	^root transactions ! !


!PortfolioRoot methodsFor: 'initialization' stamp: 'JP 10/19/2023 16:16:09'!
initializeWith: anEntity
	
	root := anEntity.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PortfolioRoot class' category: 'Portfolio-Ejercicio'!
PortfolioRoot class
	instanceVariableNames: ''!

!PortfolioRoot class methodsFor: 'as yet unclassified' stamp: 'JP 10/19/2023 15:56:26'!
with: anEntity
	
	^self new initializeWith: anEntity! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
FinanceService subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions entityParent'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'NR 10/17/2019 15:06:56'!
initialize

	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'JP 10/19/2023 00:29:06'!
balance

	^transactions inject: 0 into: [ :totalBalance :transaction | transaction addToBalance: totalBalance ] .! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'JP 10/19/2023 14:13:40'!
includes: anEntity
	
	^self = anEntity.! !


!ReceptiveAccount methodsFor: 'associating' stamp: 'JP 10/19/2023 15:33:59'!
addParent: anEntityParent
	
	^entityParent := anEntityParent.! !
