!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 22:21:28'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:02:28'!
addToEntero: a1stAdderEntero

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:04:23'!
addToFraccion: a1stAdderFraccion

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:13:42'!
divideToEntero: aDividendEntero

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:14:06'!
divideToFraccion: aDividendFraccion

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 13:32:50'!
multiplicacionCruzadaEntreFraccion:aMultiplierFraccion yEntero: aMultiplierEntero
	
	^aMultiplierEntero * aMultiplierFraccion numerator / aMultiplierFraccion denominator.! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:16:06'!
multiplyToEntero: a1stMultiplierEntero

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:16:09'!
multiplyToFraccion: a1stMultiplierFraccion

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:16:13'!
substractAFraccion: a1stSubtrahendFraccion

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:16:16'!
substractAnEntero: a1stSubtrahendEntero

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 13:33:20'!
sumaCruzadaEntreFraccion: aFraccion yEntero: anEntero

	
	^((aFraccion denominator * anEntero ) + aFraccion numerator )/ aFraccion denominator.! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:09'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de número inválido!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 12:47:44'!
* aMultiplier 
	
	^aMultiplier multiplyToEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:21:28'!
+ anAdder 

	^anAdder addToEntero: self.
	
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:34:58'!
- aSubtrahend 
	
	^ aSubtrahend substractAnEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 14:58:16'!
/ aDivisor 

	^aDivisor divideToEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 23:09:29'!
// aDivisor 
	
	^Entero with: value // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:00:11'!
addToEntero: a1stAdderEntero

	^self class with: (value + a1stAdderEntero  integerValue ) .

	
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:18:33'!
addToFraccion: a1stAdderFraccion

	^super sumaCruzadaEntreFraccion: a1stAdderFraccion yEntero: self! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:05:41'!
divideToEntero: aDividendEntero

	^ Fraccion with: aDividendEntero over: self 
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:13:05'!
divideToFraccion: aDividendFraccion

	^aDividendFraccion numerator / (self * aDividendFraccion denominator).
	

	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:41:29'!
fibonacci

	self subclassResponsibility 
		! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:00'!
greatestCommonDivisorWith: anEntero 
	
	^self class with: (value gcd: anEntero integerValue)! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 12:46:03'!
multiplyToEntero: a1stMultiplierEntero
	
	^self class with: value * a1stMultiplierEntero integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 13:30:03'!
multiplyToFraccion: a1stMultiplierFraccion
	
	^super multiplicacionCruzadaEntreFraccion: a1stMultiplierFraccion yEntero: self ! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 11:55:05'!
substractAFraccion: a1stSubtrahendFraccion

	^ a1stSubtrahendFraccion class with: a1stSubtrahendFraccion numerator - (a1stSubtrahendFraccion denominator * self) over: a1stSubtrahendFraccion denominator
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 23:53:46'!
substractAnEntero: a1stSubtrahendEntero

	^ Entero with: (a1stSubtrahendEntero integerValue - value).
! !


!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 21:01'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 20:09'!
initalizeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'testing' stamp: 'NR 9/23/2018 22:17:55'!
isNegative
	
	^value < 0! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:14'!
isOne
	
	^value = 1! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:12'!
isZero
	
	^value = 0! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'NR 4/15/2021 16:42:24'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no está definido aquí para Enteros Negativos!!!!!!'! !

!Entero class methodsFor: 'instance creation' stamp: 'MG 9/13/2023 22:53:34'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	aValue == 0 ifTrue: [^Cero with:aValue].
	aValue = 1 ifTrue: [^Uno with: aValue].
	aValue < 0 ifTrue: [^NaturalOpuesto with: aValue].
	^NaturalMayorAUno with: aValue.! !


!classDefinition: #Cero category: 'Numero-Exercise'!
Entero subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!
!Cero commentStamp: '<historical>' prior: 0!
Entero with: 0!


!Cero methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 16:00:00'!
fibonacci
	
	^ Entero with:1.! !


!Cero methodsFor: 'initialization' stamp: 'MG 9/13/2023 22:29:01'!
initalizeWith: aValue

	value:= aValue ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cero class' category: 'Numero-Exercise'!
Cero class
	instanceVariableNames: ''!

!Cero class methodsFor: 'instance creation' stamp: 'MG 9/13/2023 22:28:33'!
with: aValue

^self new initalizeWith: aValue! !


!classDefinition: #Natural category: 'Numero-Exercise'!
Entero subclass: #Natural
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Natural methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 16:01:06'!
fibonacci

	self subclassResponsibility 
		! !


!classDefinition: #NaturalMayorAUno category: 'Numero-Exercise'!
Natural subclass: #NaturalMayorAUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NaturalMayorAUno methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 16:00:51'!
fibonacci

	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.
	
	^ (self - one) fibonacci + (self - two) fibonacci! !


!NaturalMayorAUno methodsFor: 'initialization' stamp: 'MG 9/13/2023 22:35:29'!
initalizeWith: aValue 
	
	value := aValue! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NaturalMayorAUno class' category: 'Numero-Exercise'!
NaturalMayorAUno class
	instanceVariableNames: ''!

!NaturalMayorAUno class methodsFor: 'instance creation' stamp: 'MG 9/13/2023 22:35:14'!
with: aValue

^self new initalizeWith: aValue! !


!classDefinition: #Uno category: 'Numero-Exercise'!
Natural subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 23:05:06'!
addToEntero: a1stAdderEntero

	^Entero with: (a1stAdderEntero  integerValue + value).
	! !

!Uno methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 16:00:33'!
fibonacci

	^Entero with: 1.
	! !


!Uno methodsFor: 'initialization' stamp: 'MG 9/13/2023 22:31:16'!
initalizeWith: aValue 
	
	value := aValue! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Uno class' category: 'Numero-Exercise'!
Uno class
	instanceVariableNames: ''!

!Uno class methodsFor: 'instance creation' stamp: 'MG 9/13/2023 22:30:43'!
with: aValue

^self new initalizeWith: aValue! !


!classDefinition: #NaturalOpuesto category: 'Numero-Exercise'!
Entero subclass: #NaturalOpuesto
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NaturalOpuesto methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:29:46'!
fibonacci

	^self error: Entero negativeFibonacciErrorDescription! !


!NaturalOpuesto methodsFor: 'initialization' stamp: 'MG 9/13/2023 22:33:22'!
initalizeWith: aValue 
	
	value := aValue! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NaturalOpuesto class' category: 'Numero-Exercise'!
NaturalOpuesto class
	instanceVariableNames: ''!

!NaturalOpuesto class methodsFor: 'instance creation' stamp: 'MG 9/13/2023 22:32:53'!
with: aValue

^self new initalizeWith: aValue! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 12:52:26'!
* aMultiplier 
	
	^aMultiplier multiplyToFraccion: self.
	
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:21:45'!
+ anAdder 

	^ anAdder addToFraccion:self.
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:45:07'!
- aSubtrahend 
	
	^aSubtrahend substractAFraccion: self! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 14:58:23'!
/ aDivisor 
	
	
	^aDivisor divideToFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:20:15'!
addToEntero: a1stAdderEntero

	
	^super sumaCruzadaEntreFraccion:self yEntero: a1stAdderEntero
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:00:50'!
addToFraccion: a1stAdderFraccion

	| newNumerator newDenominator |
	
	newNumerator := (numerator * a1stAdderFraccion denominator) + (denominator * a1stAdderFraccion numerator).
	newDenominator := denominator * a1stAdderFraccion denominator.
	
	^newNumerator / newDenominator! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:00:25'!
divideToEntero: aDividendEntero

	^(self denominator * aDividendEntero ) / self numerator .! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 15:07:04'!
divideToFraccion: aDividendFraccion

	^(denominator * aDividendFraccion numerator) / (numerator * aDividendFraccion denominator)! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 13:31:22'!
multiplyToEntero: a1stMultiplierEntero
	
	^super multiplicacionCruzadaEntreFraccion: self yEntero: a1stMultiplierEntero .
	
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 12:51:14'!
multiplyToFraccion: a1stMultiplierFraccion
	
	^(numerator * a1stMultiplierFraccion numerator) / (denominator * a1stMultiplierFraccion denominator)
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/14/2023 00:03:47'!
negated

	^self class with: numerator over: (denominator * (Entero with:-1)).
	
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 09:56:54'!
substractAFraccion: a1stSubtrahendFraccion
	
	| newNumerator newDenominator |
	
	newNumerator :=  (denominator * a1stSubtrahendFraccion numerator) - (numerator * a1stSubtrahendFraccion denominator).
	newDenominator := denominator * a1stSubtrahendFraccion denominator.
	
	^newNumerator / newDenominator 
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MG 9/13/2023 12:53:32'!
substractAnEntero: a1stSubtrahendEntero

	^self class with: (a1stSubtrahendEntero * self denominator) - self numerator over: self denominator.
	
! !


!Fraccion methodsFor: 'comparing' stamp: 'MG 9/14/2023 00:15:04'!
= anObject

	^(anObject isKindOf: self class) and: [ (Entero with: numerator integerValue * anObject denominator integerValue) = (Entero with: (denominator integerValue * anObject numerator integerValue) )]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'NR 9/23/2018 23:45:19'!
with: aDividend over: aDivisor

	| greatestCommonDivisor numerator denominator |
	
	aDivisor isZero ifTrue: [ self error: self canNotDivideByZeroErrorDescription ].
	aDividend isZero ifTrue: [ ^aDividend ].
	
	aDivisor isNegative ifTrue:[ ^aDividend negated / aDivisor negated].
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: aDivisor. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := aDivisor // greatestCommonDivisor.
	
	denominator isOne ifTrue: [ ^numerator ].

	^self new initializeWith: numerator over: denominator
	! !
