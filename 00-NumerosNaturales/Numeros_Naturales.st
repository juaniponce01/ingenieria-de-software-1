!classDefinition: #I category: 'Numeros Naturales'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'Numeros Naturales'!
I class
	instanceVariableNames: 'next'!

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:14:03'!
* aNaturalNumber
	^aNaturalNumber! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:14:01'!
+ aNaturalNumber
	^ aNaturalNumber next! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 17:59:02'!
- aNaturalNumber
	^self error: self descripcionDeErrorDeNumerosNegativosNoSoportados.! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:00:30'!
/ aNaturalNumber
	aNaturalNumber = I ifTrue: [^self].
	^self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor.! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:47:10'!
< aNaturalNumber
	^aNaturalNumber isNotEqualTo: I.! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:11:57'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'El divisor no puede ser mayor al dividendo'
	! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:11:34'!
descripcionDeErrorDeNumerosNegativosNoSoportados.

	^'El sustraendo no puede ser mayor o igual que el minuendo'! !

!I class methodsFor: 'aritmetica' stamp: 'MH 8/24/2023 21:00:46'!
next
	next ifNotNil: [^next].
	next := self cloneNamed: self name, 'I'.
	^next.! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:14:17'!
restateA: aNaturalNumber
	^ aNaturalNumber previous! !

!I class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:18:52'!
timesToDivide: aNaturalNumber
    ^aNaturalNumber! !


!I class methodsFor: '--** private fileout/in **--' stamp: 'JP 9/3/2023 18:54:49'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := II.! !


!classDefinition: #II category: 'Numeros Naturales'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'Numeros Naturales'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:17:08'!
* aNaturalNumber
	^aNaturalNumber  + (self previous * aNaturalNumber).! !

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:13:47'!
+ aNaturalNumber
	^ self previous + aNaturalNumber next.! !

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:15:01'!
- aNaturalNumber
	^ aNaturalNumber restateA: self! !

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:18:13'!
/ aNaturalNumber
	^ aNaturalNumber timesToDivide: self! !

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:40:36'!
< aNaturalNumber

	aNaturalNumber = I ifTrue: [^false].
	^self previous < aNaturalNumber previous! !

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:26:38'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'El divisor no puede ser mayor que el dividendo'! !

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:15:56'!
next
    next ifNil: [
        next := II createChildNamed:  self name , 'I'.
        next previous: self ].
    ^ next.! !

!II class methodsFor: 'aritmetica' stamp: 'MH 8/24/2023 21:21:23'!
previous
	^ previous! !

!II class methodsFor: 'aritmetica' stamp: 'MH 8/24/2023 21:21:03'!
previous: aNaturalNumber
	^ previous := aNaturalNumber! !

!II class methodsFor: 'aritmetica' stamp: 'MH 8/24/2023 21:58:33'!
restateA: aNaturalNumber
	^ aNaturalNumber previous - self previous! !

!II class methodsFor: 'aritmetica' stamp: 'JP 9/3/2023 18:50:19'!
timesToDivide: aNaturalNumber

	aNaturalNumber = self ifTrue: [^I].
	aNaturalNumber < self ifTrue: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	aNaturalNumber - self < self ifTrue:[^I].
	^aNaturalNumber - self / self + I.! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'JP 9/3/2023 18:54:49'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: 'Numeros Naturales'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'Numeros Naturales'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'JP 9/3/2023 18:54:49'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	previous := II.! !


!classDefinition: #IIII category: 'Numeros Naturales'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: 'Numeros Naturales'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: '--** private fileout/in **--' stamp: 'JP 9/3/2023 18:54:49'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIII.
	previous := III.! !


!classDefinition: #IIIII category: 'Numeros Naturales'!
II subclass: #IIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIII class' category: 'Numeros Naturales'!
IIIII class
	instanceVariableNames: ''!

!IIIII class methodsFor: '--** private fileout/in **--' stamp: 'JP 9/3/2023 18:54:49'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIII.
	previous := IIII.! !


!classDefinition: #IIIIII category: 'Numeros Naturales'!
II subclass: #IIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIII class' category: 'Numeros Naturales'!
IIIIII class
	instanceVariableNames: ''!

!IIIIII class methodsFor: '--** private fileout/in **--' stamp: 'JP 9/3/2023 18:54:49'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := IIIII.! !

I initializeAfterFileIn!
II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!
IIIII initializeAfterFileIn!
IIIIII initializeAfterFileIn!