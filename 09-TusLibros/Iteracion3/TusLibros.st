!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 18:09'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:50'!
test01CanNotCheckoutAnEmptyCart

	| salesBook |
	
	salesBook := OrderedCollection new.
	self 
		should: [ Cashier 
			toCheckout: testObjectsFactory createCart 
			charging: testObjectsFactory notExpiredCreditCard 
			throught: self
			on: testObjectsFactory today
			registeringOn:  salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier cartCanNotBeEmptyErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test02CalculatedTotalIsCorrect

	| cart cashier |
	
	cart := testObjectsFactory createCart.
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	
	cashier :=  Cashier
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard 
		throught: self
		on: testObjectsFactory today 
		registeringOn: OrderedCollection new.
		
	self assert: cashier checkOut = (testObjectsFactory itemSellByTheStorePrice * 2)! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test03CanNotCheckoutWithAnExpiredCreditCart

	| cart salesBook |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
	
	self
		should: [ Cashier 
				toCheckout: cart 
				charging: testObjectsFactory expiredCreditCard 
				throught: self
				on: testObjectsFactory today
				registeringOn: salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | 
			self assert: anError messageText = Cashier canNotChargeAnExpiredCreditCardErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:04'!
test04CheckoutRegistersASale

	| cart cashier salesBook total |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	total := cashier checkOut.
					
	self assert: salesBook size = 1.
	self assert: salesBook first total = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:00'!
test05CashierChargesCreditCardUsingMerchantProcessor

	| cart cashier salesBook total creditCard debitedAmout debitedCreditCard  |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	debitBehavior := [ :anAmount :aCreditCard | 
		debitedAmout := anAmount.
		debitedCreditCard := aCreditCard ].
	total := cashier checkOut.
					
	self assert: debitedCreditCard = creditCard.
	self assert: debitedAmout = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:59'!
test06CashierDoesNotSaleWhenTheCreditCardHasNoCredit

	| cart cashier salesBook creditCard |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 	debitBehavior := [ :anAmount :aCreditCard | self error: Cashier creditCardHasNoCreditErrorMessage].
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	self 
		should: [cashier checkOut ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier creditCardHasNoCreditErrorMessage.
			self assert: salesBook isEmpty ]! !


!CashierTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 19:03'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'merchant processor protocol' stamp: 'HernanWilkinson 6/17/2013 19:02'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #EntryInterfaceTest category: 'TusLibros'!
TestCase subclass: #EntryInterfaceTest
	instanceVariableNames: 'authenticatorBehaviour testObjectFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!EntryInterfaceTest methodsFor: 'tests' stamp: 'MG 11/7/2023 17:10:12'!
dataBaseIncludes: aClientID withPassword: aPassword

	^authenticatorBehaviour value: aClientID value: aPassword ! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 13:41:50'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 11:47:27'!
failWithCartDoesNotExistErrorMessageDoing: aClosureExpectedToFail on: anEntryInterface

	self 
		should: aClosureExpectedToFail 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | 
			self assert: anError messageText = anEntryInterface cartDoesNotExistErrorMessage ]! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 13:41:14'!
setUp 

	testObjectFactory := StoreTestObjectsFactory new.
	authenticatorBehaviour := [ :anAmount :aCreditCard | true ].
	debitBehavior := [ :anAmount :aCreditCard | ].! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 12:43:21'!
test01CreateCartWithInvalidUserShouldntCreateACart

	| entryInterface cartID |
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog .
	
	authenticatorBehaviour := [ :clientID :password | self error: entryInterface authenticationFailedErrorMessage ].
	
	self 
		should: [ cartID := entryInterface 
					createCartForClientID: #InvalidClientID 
					withCatalog: testObjectFactory defaultCatalog 
					password: #InvalidPassword 
					throughAuthenticator: self ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: entryInterface authenticationFailedErrorMessage.
			self failWithCartDoesNotExistErrorMessageDoing: [ entryInterface listCart: cartID ] on: entryInterface ]! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 12:43:43'!
test02CreateCartWithValidUserShouldCreateACart

	| entryInterface cartID | 
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog.
	
	self shouldntFail:  [ cartID := entryInterface 
						createCartForClientID: #ValidClientID 
						withCatalog: testObjectFactory  defaultCatalog 
						password: #ClientPassword 
						throughAuthenticator: self].
	self assert: (entryInterface listCart: cartID) isEmpty.! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 14:39:37'!
test03InterfaceShouldAddAnItemToCartWithValidISBN
	
	| entryInterface cartID aQuantity anISBN |
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog .
	cartID := entryInterface 
		createCartForClientID: #ValidClientID 
		withCatalog: testObjectFactory defaultCatalog 
		password: #ClientPassword 
		throughAuthenticator: self.
						
	anISBN := testObjectFactory validItemISBN.
	aQuantity := 1.
	
	self shouldntFail: [entryInterface addToCart: cartID bookISBN: anISBN quantity: aQuantity].
	self assert: (entryInterface listCart: cartID) equals: (OrderedCollection with: testObjectFactory validItemISBN with: 1).
	! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 13:04:50'!
test04InterfaceShouldNotAddAnItemToCartWithInvalidISBN

	| entryInterface cartID aQuantity anISBN |
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog.
	cartID := entryInterface 
		createCartForClientID: #ValidClientID 
		withCatalog: testObjectFactory defaultCatalog 
		password: #ClientPassword 
		throughAuthenticator: self.
		
	anISBN := testObjectFactory invalidItemISBN.
	aQuantity := 1.
	
	self 
		should: [entryInterface addToCart: cartID bookISBN: anISBN quantity: aQuantity]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: entryInterface invalidISBNErrorMessage ].
	! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 14:27:17'!
test05InterfaceCanListCart

	| entryInterface cartID aQuantity anISBN |
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog.
	cartID := entryInterface 
		createCartForClientID: #ValidClientID 
		withCatalog: testObjectFactory  defaultCatalog 
		password: #ClientPassword 
		throughAuthenticator: self.
	anISBN := testObjectFactory validItemISBN .
	aQuantity := 1.
	entryInterface addToCart: cartID bookISBN: anISBN quantity: aQuantity.
	
	self assert: (entryInterface listCart: cartID) equals: (OrderedCollection with: anISBN with: aQuantity).
	! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 13:42:54'!
test06InterfaceCanCheckOut

	| entryInterface cartID aQuantity anISBN transactionID creditCard salesBook | 
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog.
		
	cartID := entryInterface 
		createCartForClientID: #ValidClientID 
		withCatalog: testObjectFactory  defaultCatalog 
		password: #ClientPassword 
		throughAuthenticator: self.
		
	anISBN := testObjectFactory validItemISBN .
	aQuantity := 3.
	entryInterface addToCart: cartID bookISBN: anISBN quantity: aQuantity.
	creditCard := testObjectFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
	
	self shouldntFail: [ transactionID := entryInterface 
						checkOutCart: cartID 
						creditCardNumber: creditCard number 
						creditCardExpiredDate: creditCard expiredDate 
						creditCardOwner: creditCard owner
						throughMerchantProcessor: self
						on: testObjectFactory today
						withSalesBook: salesBook ].
	self assert: transactionID equals: #TransactionID.
	
	! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 14:06:46'!
test07InterfaceCanListPurchases

	| entryInterface cartID aQuantity anISBN transactionID creditCard salesBook purchases | 
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog.
		
	cartID := entryInterface 
		createCartForClientID: #ValidClientID 
		withCatalog: testObjectFactory  defaultCatalog 
		password: #ClientPassword 
		throughAuthenticator: self.
		
	anISBN := testObjectFactory validItemISBN .
	aQuantity := 1.
	entryInterface addToCart: cartID bookISBN: anISBN quantity: aQuantity.
	creditCard := testObjectFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
	transactionID := entryInterface 
		checkOutCart: cartID 
		creditCardNumber: creditCard number 
		creditCardExpiredDate: creditCard expiredDate 
		creditCardOwner: creditCard owner
		throughMerchantProcessor: self
		on: testObjectFactory today
		withSalesBook: salesBook.
		
	self shouldntFail: [ purchases := entryInterface listPurchasesOfClientID: #ValidClientID withPassword: #ClientPassword ].
	self 
		assert: purchases 
		equals: 
			(OrderedCollection 
				with: testObjectFactory validItemISBN 
				with: aQuantity 
				with: testObjectFactory itemSellByTheStorePrice).
	
	! !

!EntryInterfaceTest methodsFor: 'tests' stamp: 'JP 11/9/2023 14:57:38'!
test08InterfaceCanListPurchasesOfClientWithManyCheckOuts

	| entryInterface cartID aQuantity anISBN transactionID creditCard salesBook purchases cartID2 anotherQuantity transactionID2 | 
	
	entryInterface := EntryInterface withISBNCatalog: testObjectFactory defaultISBNCatalog.
		
	cartID := entryInterface 
		createCartForClientID: #ValidClientID 
		withCatalog: testObjectFactory  defaultCatalog 
		password: #ClientPassword 
		throughAuthenticator: self.
		
	anISBN := testObjectFactory validItemISBN .
	aQuantity := 1.
	
	entryInterface addToCart: cartID bookISBN: anISBN quantity: aQuantity.
	
	creditCard := testObjectFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
	transactionID := entryInterface 
		checkOutCart: cartID 
		creditCardNumber: creditCard number 
		creditCardExpiredDate: creditCard expiredDate 
		creditCardOwner: creditCard owner
		throughMerchantProcessor: self
		on: testObjectFactory today
		withSalesBook: salesBook.
		
	cartID2 := entryInterface 
		createCartForClientID: #ValidClientID 
		withCatalog: testObjectFactory defaultCatalog 
		password: #ClientPassword 
		throughAuthenticator: self.
		
	anotherQuantity := 2.
		
	entryInterface addToCart: cartID2 bookISBN: anISBN quantity: anotherQuantity.
	
	transactionID2 := entryInterface 
		checkOutCart: cartID2
		creditCardNumber: creditCard number 
		creditCardExpiredDate: creditCard expiredDate 
		creditCardOwner: creditCard owner
		throughMerchantProcessor: self
		on: testObjectFactory today
		withSalesBook: salesBook.
		
	self shouldntFail: [ purchases := entryInterface listPurchasesOfClientID: #ValidClientID withPassword: #ClientPassword ].
	self 
		assert: purchases 
		equals: 
			(OrderedCollection 
				with: testObjectFactory validItemISBN 
				with: aQuantity + anotherQuantity 
				with: testObjectFactory itemSellByTheStorePrice * 3).
	
	! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:06'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'total' stamp: 'MG 11/7/2023 17:34:51'!
listCart

	^ items copy.! !

!Cart methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 19:09'!
total

	^ items sum: [ :anItem | catalog at: anItem ]! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:08'!
calculateTotal

	total := cart total.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:07'!
createSale

	^ Sale of: total
! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
debitTotal

	merchantProcessor debit: total from: creditCard.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
registerSale

	salesBook add: self createSale! !


!Cashier methodsFor: 'checkout' stamp: 'HernanWilkinson 6/17/2013 19:06'!
checkOut

	self calculateTotal.
	self debitTotal.
	self registerSale.

	^ total! !


!Cashier methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:53'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:22'!
assertIsNotEmpty: aCart 
	
	aCart isEmpty ifTrue: [self error: self cartCanNotBeEmptyErrorMessage ]! !

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:23'!
assertIsNotExpired: aCreditCard on: aDate
	
	(aCreditCard isExpiredOn: aDate) ifTrue: [ self error: self canNotChargeAnExpiredCreditCardErrorMessage ]! !


!Cashier class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:51'!
toCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook
	
	self assertIsNotEmpty: aCart.
	self assertIsNotExpired: aCreditCard on: aDate.
	
	^self new initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook! !


!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 18:21'!
canNotChargeAnExpiredCreditCardErrorMessage
	
	^'Can not charge an expired credit card'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:56'!
cartCanNotBeEmptyErrorMessage
	
	^'Can not check out an empty cart'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 19:02'!
creditCardHasNoCreditErrorMessage
	
	^'Credit card has no credit'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expiration number owner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'JP 11/9/2023 13:26:44'!
expiredDate
	
	^expiration! !

!CreditCard methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 18:39'!
isExpiredOn: aDate 
	
	^expiration start < (Month month: aDate monthIndex year: aDate yearNumber) start ! !

!CreditCard methodsFor: 'testing' stamp: 'JP 11/9/2023 13:24:45'!
number

	^number! !

!CreditCard methodsFor: 'testing' stamp: 'JP 11/9/2023 13:24:58'!
owner

	^owner! !


!CreditCard methodsFor: 'initialization' stamp: 'JP 11/9/2023 13:28:11'!
initializeWithNumber: aNumber expiringOn: aMonth owner: aName 

	number := aNumber.
	expiration := aMonth.
	owner := aName.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:38'!
expiringOn: aMonth 
	
	^self new initializeExpiringOn: aMonth! !

!CreditCard class methodsFor: 'instance creation' stamp: 'JP 11/9/2023 13:27:51'!
withNumber: aNumber expiringOn: aMonth owner: aName 

	^self new initializeWithNumber: aNumber expiringOn: aMonth owner: aName ! !


!classDefinition: #EntryInterface category: 'TusLibros'!
Object subclass: #EntryInterface
	instanceVariableNames: 'clientID password clientAuthenticator cartsByID itemsByISBN total cartsByClientID'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 12:07:05'!
addToCart: cartID bookISBN: anISBN quantity: aQuantity

	self checkIsValidISBN: anISBN.

	(cartsByID at: cartID) add: aQuantity of: (itemsByISBN at: anISBN).! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'MG 11/6/2023 21:02:57'!
authenticationFailedErrorMessage

	^'Invalid clientID or password'.! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'MG 11/6/2023 21:03:07'!
cartDoesNotExistErrorMessage
	
	^'CartID does not exist'.! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 12:18:20'!
checkClientID: aClientID withPassword: aPassword existsInClientAuthenticator: aClientAuthenticator

	^ (aClientAuthenticator dataBaseIncludes: aClientID withPassword: aPassword) 
		ifFalse: [ self error: self authenticationFailedErrorMessage ]! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 11:48:44'!
checkIsValidCartID: aCartID

	^ (cartsByID includesKey: aCartID) ifFalse: [ self error: self cartDoesNotExistErrorMessage ]! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 12:06:24'!
checkIsValidISBN: anISBN

	^ (itemsByISBN includesKey: anISBN) ifFalse: [ self error: self invalidISBNErrorMessage ]! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 15:01:17'!
checkOutCart: aCartID 	creditCardNumber: aNumber creditCardExpiredDate: anExpirationDate creditCardOwner: anOwnerName throughMerchantProcessor: aMerchantProcessor on: aDate withSalesBook: aSalesBook

	| cashier |
	cashier := Cashier 
		toCheckout: (cartsByID at: aCartID) 
		charging: (CreditCard withNumber: aNumber expiringOn: 	anExpirationDate owner: anOwnerName)
		throught: aMerchantProcessor 
		on: aDate
		registeringOn: aSalesBook.
		
	total := total + cashier checkOut.
	
	^#TransactionID.
	! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 14:20:05'!
createCartForClientID: aClientID withCatalog: aCatalog password: aPassword throughAuthenticator: aClientAuthenticator
	|aCart cartID|
	self checkClientID: aClientID withPassword: aPassword existsInClientAuthenticator: aClientAuthenticator.
	aCart := Cart acceptingItemsOf: aCatalog.
	cartID:= 1.
	cartsByID at: cartID put: aCart.
	cartsByClientID at: aClientID put: aCart.
	
	^cartID.! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 12:06:52'!
invalidISBNErrorMessage

	^'The ISBN provided does not exists'.! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 14:40:44'!
listCart: aCartID

	| items list cart |
	self checkIsValidCartID: aCartID.
	
	list := OrderedCollection new.
	
	cart := (cartsByID at: aCartID).
	items := cart listCart asSet. 
	
	items do: [ :item | list add: (itemsByISBN keyAtValue: item). list add: (cart occurrencesOf: item) ].
	
	^list ! !

!EntryInterface methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 14:47:43'!
listPurchasesOfClientID: aClientID withPassword: aPassword

	| list |
	list := (self listCart: (cartsByID keyAtValue: (cartsByClientID at: aClientID))).
	list add: total.
	^list
	
	! !


!EntryInterface methodsFor: 'initialization' stamp: 'JP 11/9/2023 15:01:27'!
initializeWithISBNCatalog: anISBNCatalog 
	
	cartsByID := Dictionary new.
	cartsByClientID := Dictionary new.
	itemsByISBN := anISBNCatalog.
	total := 0.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EntryInterface class' category: 'TusLibros'!
EntryInterface class
	instanceVariableNames: ''!

!EntryInterface class methodsFor: 'instance creation' stamp: 'JP 11/9/2023 12:41:50'!
withISBNCatalog: anISBNCatalog 
	
	^self new initializeWithISBNCatalog: anISBNCatalog ! !


!classDefinition: #Sale category: 'TusLibros'!
Object subclass: #Sale
	instanceVariableNames: 'total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 18:48'!
total
	
	^ total! !


!Sale methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:47'!
initializeTotal: aTotal

	total := aTotal ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: 'TusLibros'!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:47'!
of: aTotal

	"should assert total is not negative or 0!!"
	^self new initializeTotal: aTotal ! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStorePrice
	
	^10! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
defaultCatalog
	
	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice;
		yourself ! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'JP 11/9/2023 13:25:38'!
expiredCreditCard
	
	^CreditCard
		withNumber: '4336-2534-1200-0994'
		expiringOn: (Month month: today monthIndex year: today yearNumber - 1)
		owner: 'Pepito'! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'JP 11/9/2023 13:23:16'!
notExpiredCreditCard
	
	^CreditCard 
		withNumber: '4336-2534-1200-0994'
		expiringOn: (Month month: today monthIndex year: today yearNumber + 1)
		owner: 'Pepito'
		! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:37'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !


!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 12:36:59'!
defaultISBNCatalog
	
	^ Dictionary new
		at: self validItemISBN put: self itemSellByTheStore;
		yourself ! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 12:57:46'!
invalidItemISBN
	
	^'0000-000-0000-0000'.! !

!StoreTestObjectsFactory methodsFor: 'as yet unclassified' stamp: 'JP 11/9/2023 12:02:02'!
validItemISBN
	
	^'2000-300-4550-2332'.! !
