!classDefinition: #CartTest category: 'TusLibros-Solucion'!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Solucion'!

!CartTest methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 19:10:21'!
test01CartStartsEmpty

	|aCart|
	aCart := Cart new.
	self assert: aCart isEmpty.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 20:48:23'!
test02CartIsNotEmptyWhenAddingABook

	|aCart aBook aPublisherBook |
	aPublisherBook := #(#Book).
	aCart := Cart withPublisherBookList: aPublisherBook .
	aBook := #Book.
	aCart add: aBook.
	
	self deny: aCart isEmpty.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 20:48:58'!
test03CartCanAddManyBooks

	|aCart book1 book2 book3 aPublisherBook |
	aPublisherBook := #(#Book1 #Book2 #Book3).
	aCart := Cart withPublisherBookList: aPublisherBook .
	book1 := #Book1.
	book2 := #Book2.
	book3 := #Book3. 
	aCart add: book1.
	aCart add: book2.
	aCart add: book3.
	
	self assert: aCart totalQuantity = 3.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 20:49:15'!
test04CartKnowsTheQuantityOfItsBooks

	|aCart book1 book2 aPublisherBook |
	aPublisherBook := #(#Book1 #Book2 #Book3).
	aCart := Cart withPublisherBookList: aPublisherBook.
	book1 := #Book1.
	book2 := #Book2. 
	aCart add: book1.
	aCart add: book1.
	aCart add: book2.
	
	self assert: (aCart quantityOfBook: book1) = 2.
	self assert: (aCart quantityOfBook: book2) = 1.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 20:49:23'!
test05CartCanListItsBooks

	|aCart book1 book2 book3 bookDictionary aPublisherBook |
	aPublisherBook := #(#Book1 #Book2 #Book3).
	aCart := Cart withPublisherBookList: aPublisherBook.
	book1 := #Book1.
	book2 := #Book2. 
	book3 := #Book3.
	aCart add: book1.
	aCart add: book1.
	aCart add: book2.
	aCart add: book2.
	aCart add: book2.
	aCart add: book3.
	
	bookDictionary := Dictionary new.
	bookDictionary at: book1 put: 2.
	bookDictionary at: book2 put: 3.
	bookDictionary at: book3 put: 1.
	
	self assert: aCart listCart = bookDictionary .! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 20:42:04'!
test06CartCannotAddABookFromUnknownPublisher

	|aCart unknownPublisherBook aPublisherBookList|
	
	aPublisherBookList := #(#Book1 #Book2 #Book3).
	aCart := Cart withPublisherBookList: aPublisherBookList .
	unknownPublisherBook := #Book4.
	
	self 
		should: [aCart add: unknownPublisherBook]
		raise:  Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | self assert: Cart cantAddBookFromUnknownPublisherErrorDescription equals: anError messageText].
			! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 21:33:55'!
test07CartCannotAddLessThanOneBook

	|aCart aPublisherBookList|
	
	aPublisherBookList := #(#Book1 #Book2).
	aCart := Cart withPublisherBookList: aPublisherBookList .
	
	self 
		should: [aCart add: #Book1 quantity: 0]
		raise:  Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | self assert: Cart cantAddLessThanOneBookErrorDescription equals: anError messageText].
			! !


!classDefinition: #Cart category: 'TusLibros-Solucion'!
Object subclass: #Cart
	instanceVariableNames: 'books publisherBookList'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Solucion'!

!Cart methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 21:38:34'!
add: aBook 

	self add: aBook quantity: 1.! !

!Cart methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 21:37:51'!
add: aBook quantity: aQuantityOfBook 
	
	self checkIsValidQuantityOfBook: aQuantityOfBook.

	self checkIsBookFromPublicherBookList: aBook.
	
	^books at: aBook put: (self quantityOfBook: aBook) + aQuantityOfBook.! !

!Cart methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 20:21:42'!
initialize
	
	books := Dictionary new.! !

!Cart methodsFor: 'as yet unclassified' stamp: 'MG 10/30/2023 19:16:14'!
isEmpty

	^ books isEmpty.! !


!Cart methodsFor: 'accessing' stamp: 'MG 10/30/2023 20:26:35'!
quantityOfBook: aBook 
	
	^books at: aBook ifAbsent: 0.! !


!Cart methodsFor: 'testing' stamp: 'MG 10/30/2023 21:37:51'!
checkIsBookFromPublicherBookList: aBook

	^ (publisherBookList includes: aBook) ifFalse: [self error: self class cantAddBookFromUnknownPublisherErrorDescription]! !

!Cart methodsFor: 'testing' stamp: 'MG 10/30/2023 21:37:14'!
checkIsValidQuantityOfBook: aQuantityOfBook

	^ aQuantityOfBook >= 1 ifFalse: [self error: self class cantAddLessThanOneBookErrorDescription]! !

!Cart methodsFor: 'testing' stamp: 'MG 10/30/2023 19:47:14'!
totalQuantity
	
	^books size.! !


!Cart methodsFor: 'associating' stamp: 'MG 10/30/2023 20:21:28'!
listCart
	
	^books copy! !


!Cart methodsFor: 'initialization' stamp: 'MG 10/30/2023 20:43:03'!
initializeWithPublisherBookList: aPublisherBookList 
	
	publisherBookList := aPublisherBookList.
	books := Dictionary new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros-Solucion'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'MG 10/30/2023 20:46:16'!
cantAddBookFromUnknownPublisherErrorDescription 
	
	^'Cannot add book from unkown publisher' ! !

!Cart class methodsFor: 'instance creation' stamp: 'MG 10/30/2023 21:36:11'!
cantAddLessThanOneBookErrorDescription 
	
	^'Cannot add less than one book' ! !

!Cart class methodsFor: 'instance creation' stamp: 'MG 10/30/2023 20:42:25'!
withPublisherBookList: aPublisherBookList 
	
	^self new initializeWithPublisherBookList: aPublisherBookList ! !
