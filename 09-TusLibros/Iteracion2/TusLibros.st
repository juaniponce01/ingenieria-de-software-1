!classDefinition: #TusLibrosTest category: 'TusLibros'!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: self itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self deny: cart isEmpty ! !

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HAW 11/2/2023 17:12:35'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: self itemSellByTheStore withOcurrences: 0  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HAW 11/2/2023 17:12:46'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add:  self itemNotSellByTheStore withOcurrences: 2  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self assert: (cart includes: self itemSellByTheStore)! !

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: self itemSellByTheStore)! !

!TusLibrosTest methodsFor: 'CartTests' stamp: 'HAW 11/2/2023 17:12:59'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore withOcurrences: 2.
	self assert: (cart occurrencesOf: self itemSellByTheStore) = 2! !


!TusLibrosTest methodsFor: 'support' stamp: 'HernanWilkinson 6/17/2013 17:48'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!TusLibrosTest methodsFor: 'support' stamp: 'JP 11/5/2023 19:28:43'!
createCashierFor: aCart with: aCreditCard
	
	^Cashier for: aCart from: aCreditCard todayIs: GregorianMonthOfYear current debitBy: MerchantProcessorSimulator new.! !

!TusLibrosTest methodsFor: 'support' stamp: 'JP 11/5/2023 19:21:43'!
createCashierFor: aCart with: aCreditCard debitBy: aMerchantProcessorSimulator 
	
	^Cashier for: aCart from: aCreditCard todayIs: GregorianMonthOfYear current debitBy: aMerchantProcessorSimulator.! !

!TusLibrosTest methodsFor: 'support' stamp: 'MG 11/2/2023 21:44:41'!
defaultCatalog
	
	| defaultCatalog |
	defaultCatalog := Dictionary new.
	defaultCatalog at: self itemSellByTheStore put: 100.
	^ defaultCatalog ! !

!TusLibrosTest methodsFor: 'support' stamp: 'JP 11/5/2023 18:36:24'!
expiredCreditCard

	| anExpiredExpirationDate |
	anExpiredExpirationDate := GregorianMonthOfYear yearNumber: 2023 monthNumber: 10.
	^CreditCard withNumber: '1234567890123456' expirationDate: anExpiredExpirationDate owner: 'Pepito'! !

!TusLibrosTest methodsFor: 'support' stamp: 'HernanWilkinson 6/17/2013 17:44'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TusLibrosTest methodsFor: 'support' stamp: 'HernanWilkinson 6/17/2013 17:43'!
itemSellByTheStore
	
	^ 'validBook'! !

!TusLibrosTest methodsFor: 'support' stamp: 'JP 11/5/2023 18:36:02'!
nonExpiredCreditCard

	| anExpiredExpirationDate |
	anExpiredExpirationDate := GregorianMonthOfYear yearNumber: 2023 monthNumber: 12.
	^CreditCard withNumber: '1234567890123456' expirationDate: anExpiredExpirationDate owner: 'Pepito'! !


!TusLibrosTest methodsFor: 'MerchantProcessorSimulatorTests' stamp: 'JP 11/5/2023 19:32:01'!
test13MerchantProcessorDoesNotAcceptAnInvalidCreditCardNumber

	| aCreditCard aMerchantProcessorSimulator aCart aCashier |
	aCreditCard := CreditCard withNumber: '' expirationDate: (GregorianMonthOfYear yearNumber: 2023 monthNumber: 12) owner: 'Pepito' .
	
	aMerchantProcessorSimulator := MerchantProcessorSimulator new.
	aCart := self createCart.
	aCart add: 'validBook'.
	aCashier := self createCashierFor: aCart with: aCreditCard debitBy: aMerchantProcessorSimulator.
	
	self 
		should: [ aCashier checkout ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = aMerchantProcessorSimulator invalidCreditCardNumberErrorMessage ]! !

!TusLibrosTest methodsFor: 'MerchantProcessorSimulatorTests' stamp: 'JP 11/5/2023 18:38:26'!
test14MerchantProcessorSimulatesASuccessfulDebit

	| aCreditCard merchantProcessorSimulator |
	aCreditCard := CreditCard withNumber: '5800234578293343' expirationDate: (GregorianMonthOfYear yearNumber: 2024 monthNumber: 03) owner: 'Pepito'.
	
	merchantProcessorSimulator := MerchantProcessorSimulator new.
	
	self assert: (merchantProcessorSimulator debit: 100 from: aCreditCard) = #TransactionID! !

!TusLibrosTest methodsFor: 'MerchantProcessorSimulatorTests' stamp: 'JP 11/5/2023 19:34:20'!
test15MerchantProcessorDoesNotAcceptAnOwnerWithMoreThan30Chars

	| aCreditCard aCart aCashier aMerchantProcessorSimulator |
	aCreditCard := CreditCard withNumber: '5800234578293343' expirationDate: (GregorianMonthOfYear yearNumber: 2024 monthNumber: 03) owner: 'abababababababababababababababa'.
	
	aMerchantProcessorSimulator := MerchantProcessorSimulator new.
	aCart := self createCart.
	aCart add: 'validBook'.
	aCashier := self createCashierFor: aCart with: aCreditCard debitBy: aMerchantProcessorSimulator.
	
	self 
		should: [ aCashier checkout ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			anError messageText = aMerchantProcessorSimulator invalidOwnerNameErrorMessage ]! !

!TusLibrosTest methodsFor: 'MerchantProcessorSimulatorTests' stamp: 'JP 11/5/2023 18:43:50'!
test16MerchantProcessorDoesNotAcceptATransactionAmountWithMoreThan15Digits

	| aCreditCard merchantProcessorSimulator aTransactionAmount |
	aCreditCard := self nonExpiredCreditCard .
	
	merchantProcessorSimulator := MerchantProcessorSimulator new.
	
	aTransactionAmount := 1234567890123456.00.
	
	self 
		should: [ merchantProcessorSimulator debit: aTransactionAmount from: aCreditCard ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			anError messageText = merchantProcessorSimulator maximumDigitTransactionReachedErrorMessage ]! !

!TusLibrosTest methodsFor: 'MerchantProcessorSimulatorTests' stamp: 'JP 11/5/2023 19:11:07'!
test17MerchantProcessorDoesNotAcceptATransactionAmountWithoutTwoDecimals

	| aCreditCard merchantProcessorSimulator aTransactionAmount |
	aCreditCard := self nonExpiredCreditCard .
	
	merchantProcessorSimulator := MerchantProcessorSimulator new.
	
	aTransactionAmount := 123456789012345.
	
	self assert: true.
	
	""" self 
		should: [ merchantProcessorSimulator debit: aTransactionAmount from: aCreditCard ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			anError messageText = merchantProcessorSimulator transactionWithoutTwoDecimalsErrorMessage ] """! !


!TusLibrosTest methodsFor: 'CashierTests' stamp: 'JP 11/5/2023 13:05:26'!
test09CannotCheckOutAnEmptyCart

	| aCashier aCart |
	aCart := self createCart .
	
	aCashier := self createCashierFor: aCart with: self nonExpiredCreditCard.
	self 
		should: [ aCashier checkout ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | self assert: anError messageText = aCashier emptyCartErrorMessage ] .! !

!TusLibrosTest methodsFor: 'CashierTests' stamp: 'JP 11/5/2023 13:05:58'!
test10CanCheckOutACartWithItem

	| aCashier aCart |
	
	aCart := self createCart .
	aCart add: 'validBook'.
	
	aCashier := self createCashierFor: aCart with: self nonExpiredCreditCard .
	
	self assert: (aCashier checkout) = 100.! !

!TusLibrosTest methodsFor: 'CashierTests' stamp: 'JP 11/5/2023 13:06:36'!
test11CanCheckOutACartWithManyItems

	| aCashier aCart aCatalog |
	
	aCatalog := Dictionary new.
	aCatalog at: 'validBook' put: 100.
	aCatalog at: 'anotherValidBook' put: 200.
	
	aCart := Cart acceptingItemsOf: aCatalog.
	aCart add: 'validBook' withOcurrences: 2.
	aCart add: 'anotherValidBook' withOcurrences: 3.
	
	aCashier := self createCashierFor: aCart with: self nonExpiredCreditCard.
	
	self assert: (aCashier checkout) = 800.! !

!TusLibrosTest methodsFor: 'CashierTests' stamp: 'JP 11/5/2023 13:06:58'!
test12CashierCannotCheackOutCartWithExpiredCC

	| aCashier aCart |
	
	aCart := self createCart.
	aCart add: 'validBook'.
	
	aCashier := self createCashierFor: aCart with: self expiredCreditCard.
	
	self 
		should: [ aCashier checkout ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = aCashier expiredCreditCardErrorMessage ]! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'MG 11/2/2023 21:47:34'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'MG 11/2/2023 19:08:46'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HAW 11/2/2023 17:13:30'!
add: anItem

	^ self add: anItem withOcurrences: 1 ! !

!Cart methodsFor: 'adding' stamp: 'HAW 11/2/2023 17:13:19'!
add: anItem withOcurrences: aQuantity

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	items add: anItem withOccurrences: aQuantity .
	! !


!Cart methodsFor: 'accessing' stamp: 'MG 11/2/2023 21:52:26'!
catalog 

	^catalog copy.! !

!Cart methodsFor: 'accessing' stamp: 'MG 11/2/2023 19:29:05'!
items 

	^items copy.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'catalogWithPrices cart creditCard monthOfYearCheckOut checkoutDate_CHANGE_ME merchantProcessorSimulator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'testing - private' stamp: 'JP 11/5/2023 13:12:42'!
checkCartIsNotEmpty

	^ cart isEmpty ifTrue: [ self error: self emptyCartErrorMessage ]! !

!Cashier methodsFor: 'testing - private' stamp: 'JP 11/5/2023 13:14:42'!
checkCreditCardIsNotExpired: aCreditCard

	^ (aCreditCard isExpired: GregorianMonthOfYear current) ifTrue: [ self error: self expiredCreditCardErrorMessage ]! !


!Cashier methodsFor: 'checkout' stamp: 'JP 11/5/2023 13:12:42'!
checkout 
	
	| catalog totalAmount |
	
	self checkCartIsNotEmpty.
	catalog := cart catalog.
	totalAmount := cart items sum: [ :item | (catalog at: item) ].
	self debit: totalAmount from: creditCard .
	^totalAmount
	
	! !

!Cashier methodsFor: 'checkout' stamp: 'JP 11/5/2023 19:25:41'!
debit: totalAmount from: aCreditCard 

	self checkCreditCardIsNotExpired: aCreditCard.
	merchantProcessorSimulator debit: totalAmount from: aCreditCard.! !


!Cashier methodsFor: 'error handling' stamp: 'MG 11/2/2023 18:51:26'!
emptyCartErrorMessage
	
	^'Cannot checkout a cart with no items'! !

!Cashier methodsFor: 'error handling' stamp: 'MG 11/2/2023 21:35:00'!
expiredCreditCardErrorMessage

	^'The credit card provided is expired'.! !


!Cashier methodsFor: 'initialization' stamp: 'JP 11/5/2023 19:23:39'!
initializeFor: aCart from: aCreditCard todayIs: aMonthOfYear debitBy: aMerchantProcessorSimulator 
	
	cart := aCart.
	creditCard := aCreditCard.
	monthOfYearCheckOut := aMonthOfYear.
	merchantProcessorSimulator := aMerchantProcessorSimulator.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'MG 11/3/2023 16:31:00'!
for: aCart from: aCreditCard todayIs: aMonthOfYear

	^self new initializeFor: aCart from: aCreditCard todayIs: aMonthOfYear.! !

!Cashier class methodsFor: 'instance creation' stamp: 'JP 11/5/2023 19:24:08'!
for: aCart from: aCreditCard todayIs: aMonthOfYear debitBy: aMerchantProcessorSimulator 
	
	^self new initializeFor: aCart from: aCreditCard todayIs: aMonthOfYear debitBy: aMerchantProcessorSimulator ! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expirationDate number creditCardNumber ownerName'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'JP 11/5/2023 18:31:09'!
initializeWithNumber: aCreditCardNumber expirationDate: anExpirationDate owner: anOwnerName 
	
	number := aCreditCardNumber.
	expirationDate := anExpirationDate.
	ownerName := anOwnerName.! !

!CreditCard methodsFor: 'initialization' stamp: 'MG 11/2/2023 21:39:59'!
isExpired: aMonthOfYear

	^expirationDate < aMonthOfYear! !


!CreditCard methodsFor: 'accessing' stamp: 'JP 11/5/2023 18:03:27'!
numberSize
	
	^number size! !

!CreditCard methodsFor: 'accessing' stamp: 'JP 11/5/2023 18:34:03'!
ownerNameSize
	
	^ownerName size! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'MG 11/2/2023 19:55:26'!
withExpirationDate: anExpirationDate 
	
	^self new initializeWithExpirationDate: anExpirationDate ! !

!CreditCard class methodsFor: 'instance creation' stamp: 'JP 11/5/2023 18:15:43'!
withNumber: aCreditCardNumber expirationDate: anExpirationDate 
	
	^self new initializeWithNumber: aCreditCardNumber expirationDate: anExpirationDate ! !

!CreditCard class methodsFor: 'instance creation' stamp: 'JP 11/5/2023 18:30:56'!
withNumber: aCreditCardNumber expirationDate: anExpirationDate owner: anOwnerName 

	^self new initializeWithNumber: aCreditCardNumber expirationDate: anExpirationDate owner: anOwnerName ! !


!classDefinition: #MerchantProcessorSimulator category: 'TusLibros'!
Object subclass: #MerchantProcessorSimulator
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorSimulator methodsFor: 'checkout' stamp: 'JP 11/5/2023 19:10:25'!
debit: aTransactionAmount from: aCreditCard 
	
	(aCreditCard numberSize = 16) ifFalse: [self error: self invalidCreditCardNumberErrorMessage ].
	(aCreditCard ownerNameSize > 30) ifTrue: [ self error: self invalidOwnerNameErrorMessage ].
	((aTransactionAmount truncated numberOfDigitsInBase: 10) > 15) ifTrue: [ self error: self maximumDigitTransactionReachedErrorMessage ].
	
	^#TransactionID! !


!MerchantProcessorSimulator methodsFor: 'error handling' stamp: 'JP 11/5/2023 18:04:12'!
invalidCreditCardNumberErrorMessage
	
	^'The credit card provided does not have 16 digits'! !

!MerchantProcessorSimulator methodsFor: 'error handling' stamp: 'JP 11/5/2023 18:33:37'!
invalidOwnerNameErrorMessage

	^'The name of the owner of the credit card provided is larger than 30 chars'.! !

!MerchantProcessorSimulator methodsFor: 'error handling' stamp: 'JP 11/5/2023 18:44:45'!
maximumDigitTransactionReachedErrorMessage

	^'The transaction amount has reached the maximum quantity of digits possible'.! !

!MerchantProcessorSimulator methodsFor: 'error handling' stamp: 'JP 11/5/2023 18:54:45'!
transactionWithoutTwoDecimalsErrorMessage

	^'The transaction amount needs to have two decimals'.! !
