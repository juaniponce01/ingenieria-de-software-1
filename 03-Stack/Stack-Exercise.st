!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'JP 9/16/2023 21:04:52'!
test01PrefixCanNotBeEmpty

	| emptyStack sentenceFinder |
	
	emptyStack  := OOStack new.
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	self
		should: [ sentenceFinder  findSentencesWithPrefix: '' inStack: emptyStack ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix prefixEmptyErrorDescription ].
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'JP 9/16/2023 21:17:56'!
test02PrefixCanNotContainEmptySpace

	| emptyStack sentenceFinder |
	
	emptyStack  := OOStack new.
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	self
		should: [ sentenceFinder  findSentencesWithPrefix: 'Wint ' inStack: emptyStack ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix prefixWithSpaceErrorDescription ].
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'JP 9/17/2023 20:36:46'!
test03PrefixIsCaseSensitive

	| stackWithSentence sentenceFinder emptyCollection |
	
	stackWithSentence  := OOStack new.
	
	stackWithSentence  push: 'winter is coming'.
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	emptyCollection := OrderedCollection new.
	
	self assert: (sentenceFinder findSentencesWithPrefix: 'Wint' inStack: stackWithSentence) = emptyCollection .
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'JP 9/17/2023 20:58:26'!
test04PrefixMatchOnlyOneSentence

	| stackWithSentences sentenceFinder collectionWithSentenceToMatch sentenceToMatch sentenceNotToMatch|
	
	sentenceToMatch := 'Winter is coming'.
	sentenceNotToMatch := 'winning is everything'.
	
	stackWithSentences  := OOStack new.
	
	stackWithSentences push: sentenceToMatch.
	stackWithSentences push: sentenceNotToMatch.
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	collectionWithSentenceToMatch := OrderedCollection new.
	collectionWithSentenceToMatch add: sentenceToMatch.
	
	
	self assert: (sentenceFinder findSentencesWithPrefix: 'Wint' inStack: stackWithSentences) = collectionWithSentenceToMatch .
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'JP 9/17/2023 21:00:07'!
test05PrefixMatchSeveralSentences

	| stackWithSentences sentenceFinder collectionWithSentencesToMatch sentenceToMatch anotherSentenceToMatch sentenceNotToMatch|
	
	sentenceToMatch := 'Winter is coming'.
	anotherSentenceToMatch := 'Winter is here'.
	sentenceNotToMatch := 'winning is everything'.
	
	stackWithSentences  := OOStack new.
	
	stackWithSentences push: sentenceToMatch.
	stackWithSentences push: anotherSentenceToMatch.
	stackWithSentences push: sentenceNotToMatch.
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	collectionWithSentencesToMatch := OrderedCollection new.
	collectionWithSentencesToMatch add: anotherSentenceToMatch.
	collectionWithSentencesToMatch add: sentenceToMatch.
	
	
	self assert: (sentenceFinder findSentencesWithPrefix: 'Wint' inStack: stackWithSentences) = collectionWithSentencesToMatch .
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'JP 9/17/2023 20:50:51'!
test06StackMantainsOrder
	| stackWithSentences stackToCheckInitialOrder sentenceFinder sentenceToMatch anotherSentenceToMatch|
	
	sentenceToMatch := 'Winter is coming'.
	anotherSentenceToMatch := 'Winter is here'.
	
	stackWithSentences  := OOStack new.
	stackToCheckInitialOrder := OOStack new.
	
	stackWithSentences push: sentenceToMatch.
	stackWithSentences push: anotherSentenceToMatch.
	
	stackToCheckInitialOrder push: sentenceToMatch.
	stackToCheckInitialOrder push: anotherSentenceToMatch.
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	sentenceFinder findSentencesWithPrefix: 'Wint' inStack: stackWithSentences.
	
	self assert: stackWithSentences pop = stackToCheckInitialOrder pop.
	self assert: stackWithSentences pop = stackToCheckInitialOrder pop.
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'JP 9/17/2023 21:01:20'!
test07PrefixDoesNotMatchAnySentence

	| stackWithSentences sentenceFinder emptyCollection sentenceNotToMatch anotherSentenceNotToMatch|
	
	sentenceNotToMatch := 'winning is everything'.
	anotherSentenceNotToMatch := 'The winds of Winter'.
	
	stackWithSentences  := OOStack new.
	
	stackWithSentences push: sentenceNotToMatch.
	stackWithSentences push: anotherSentenceNotToMatch.
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	emptyCollection := OrderedCollection new.
	
	self assert: (sentenceFinder findSentencesWithPrefix: 'Wint' inStack: stackWithSentences) = emptyCollection .
	
	! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'lastElement size'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'JP 9/16/2023 18:44:25'!
initialize

	size := 0.
	lastElement := StackBase withStack: self.! !


!OOStack methodsFor: 'accesing' stamp: 'JP 9/16/2023 19:05:34'!
size 

	^size! !

!OOStack methodsFor: 'accesing' stamp: 'JP 9/16/2023 19:05:39'!
top

	^lastElement value.! !


!OOStack methodsFor: 'adding' stamp: 'JP 9/16/2023 20:03:09'!
push: anElementToPush
	
	size := size + 1.
	lastElement := StackElement withValue: anElementToPush andWithParent: lastElement.! !


!OOStack methodsFor: 'removing' stamp: 'JP 9/16/2023 19:35:16'!
pop

	| lastValue |
	lastValue := lastElement value.
	lastElement := lastElement parent.
	size := size - 1.
	^lastValue.! !


!OOStack methodsFor: 'error handling' stamp: 'JP 9/16/2023 19:05:20'!
raiseEmptyErrorDescription

	^self error: self class stackEmptyErrorDescription.! !


!OOStack methodsFor: 'testing' stamp: 'JP 9/16/2023 18:48:41'!
isEmpty

	^lastElement isEmpty.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/14/2023 08:12:21'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'JP 9/17/2023 22:10:21'!
extractSentencesThatMatchPrefix: aPrefixToMatch inStack: aStackToFindSentences 

	| stackToMantainOrder collectionWithPrefixMatchSentences sentence |
	
	stackToMantainOrder  := OOStack new.
	
	collectionWithPrefixMatchSentences := OrderedCollection new.

	[aStackToFindSentences isEmpty]
	whileFalse: 
		[
			sentence := aStackToFindSentences pop.
			(sentence is: aPrefixToMatch substringAt: 1) ifTrue: [collectionWithPrefixMatchSentences add: sentence].
			stackToMantainOrder push: sentence.
		].
	
	self sortStack: aStackToFindSentences toReverseOrderInStack: stackToMantainOrder.
	
	^collectionWithPrefixMatchSentences .! !

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'JP 9/17/2023 21:34:42'!
findSentencesWithPrefix: aPrefixToFind inStack: aStackWithSentences

	self verifyConditionsForPrefix: aPrefixToFind. 
	
	^self extractSentencesThatMatchPrefix: aPrefixToFind inStack: aStackWithSentences .
	! !

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'JP 9/17/2023 21:56:49'!
sortStack: aStackToSort toReverseOrderInStack: aStackToCheckOrder

	| element |

	[aStackToCheckOrder isEmpty]
	whileFalse: 
		[	
			element := aStackToCheckOrder pop.
			aStackToSort push: element.
		].! !

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'JP 9/17/2023 21:12:48'!
verifyConditionsForPrefix: aPrefixToVerify

	" estos ifs no son parte del contexto de nuestro problema por lo que creemos que son discutibles (tendriamos que subclasificar la clase de strings) "

	(aPrefixToVerify isEmpty) ifTrue: [self error: self class prefixEmptyErrorDescription].
	
	(aPrefixToVerify includesSubString: ' ') ifTrue: [self error: self class prefixWithSpaceErrorDescription].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'as yet unclassified' stamp: 'JP 9/16/2023 20:29:00'!
prefixEmptyErrorDescription

	^'El prefijo no puede ser vacio!!'.! !

!SentenceFinderByPrefix class methodsFor: 'as yet unclassified' stamp: 'JP 9/16/2023 20:28:31'!
prefixWithSpaceErrorDescription

	^'El prefijo no puede contener un espacio!!'.! !


!classDefinition: #StackComponents category: 'Stack-Exercise'!
Object subclass: #StackComponents
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackComponents methodsFor: 'as yet unclassified' stamp: 'JP 9/16/2023 19:04:44'!
isEmpty
	^self subclassResponsibility! !

!StackComponents methodsFor: 'as yet unclassified' stamp: 'JP 9/16/2023 19:04:38'!
value
	^self subclassResponsibility! !


!classDefinition: #StackBase category: 'Stack-Exercise'!
StackComponents subclass: #StackBase
	instanceVariableNames: 'associatedStack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackBase methodsFor: 'initialization' stamp: 'JP 9/16/2023 19:06:24'!
initializeWithStack: aStack

	associatedStack := aStack.! !


!StackBase methodsFor: 'accesing' stamp: 'JP 9/16/2023 19:06:21'!
value

	^associatedStack raiseEmptyErrorDescription.! !


!StackBase methodsFor: 'testing' stamp: 'JP 9/16/2023 19:06:18'!
isEmpty

	^true.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackBase class' category: 'Stack-Exercise'!
StackBase class
	instanceVariableNames: ''!

!StackBase class methodsFor: 'as yet unclassified' stamp: 'JP 9/16/2023 18:47:21'!
withStack: aStack
	
	^self new initializeWithStack: aStack.! !


!classDefinition: #StackElement category: 'Stack-Exercise'!
StackComponents subclass: #StackElement
	instanceVariableNames: 'value parent'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackElement methodsFor: 'initialization' stamp: 'JP 9/16/2023 18:51:53'!
initializeWithValue: aValue andWithParent: aParent

	value := aValue.
	parent := aParent.! !


!StackElement methodsFor: 'accesing' stamp: 'JP 9/16/2023 19:06:28'!
parent 

	^parent.! !

!StackElement methodsFor: 'accesing' stamp: 'JP 9/16/2023 19:06:32'!
value

	^value.! !


!StackElement methodsFor: 'testing' stamp: 'JP 9/16/2023 19:06:35'!
isEmpty

	^false.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackElement class' category: 'Stack-Exercise'!
StackElement class
	instanceVariableNames: ''!

!StackElement class methodsFor: 'as yet unclassified' stamp: 'JP 9/16/2023 18:52:37'!
withValue: aValue andWithParent: aParent

	^self new initializeWithValue: aValue andWithParent: aParent.! !
